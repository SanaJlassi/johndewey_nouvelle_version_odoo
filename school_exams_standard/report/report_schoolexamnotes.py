# -*- coding: utf-8 -*-
from datetime import datetime
import time
from odoo import api, models
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)


class ExamNotes(models.AbstractModel):
    _name = 'report.school_exams.report_schoolexamnotes'

    def _get_exam_types(self,educational_stage_id):
        exam_types_list = []
        exam_types_list = self.env['school.exam.type'].search([('educational_stage_id','=',educational_stage_id.id)])
        print "################### EXAM  TYPES LIST",exam_types_list
        return exam_types_list

    def _get_subject_note(self,academic_year, educational_stage_id, periode_config_id, periode_id, grade_id, class_id, subject_id, exam_type_id, student_id):
        exam_note_ids = []
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_id.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id),('exam_type_id','=',exam_type_id.id)]
        exam_note_ids = self.env['school.exam.note'].search(domain)
        if len(exam_note_ids):
            for line in exam_note_ids : 
                note_id = line.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
                notes_list.append(note_id.note)
            return str(notes_list)[1:-1]
        else:
            return '-'

    def _get_subject_average(self,academic_year, educational_stage_id, periode_config_id, periode_id, grade_id, class_id, subject_id, student_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_id.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):
            for line in notes_list : 
                note_id = line.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
                average = note_id.average
            return average
        else:
            return '-'



    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        print "##### render_html"
        academic_year                 = 'academic_year' in data['form'] and data['form']['academic_year'] or False
        educational_stage_id          = 'educational_stage_id' in data['form'] and data['form']['educational_stage_id'] or False
        periode_config_id             = 'periode_config_id' in data['form'] and data['form']['periode_config_id'] or False
        periode_ids                   = 'periode_ids' in data['form'] and data['form']['periode_ids'] or False
        grade_id                      = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        class_id                      = 'class_id' in data['form'] and data['form']['class_id'] or False
        # student_id                    = 'student_id' in data['form'] and data['form']['student_id'] or False
        subject_id                    = 'subject_id' in data['form'] and data['form']['subject_id'] or False
        exam_subject_config           = 'exam_subject_config' in data['form'] and data['form']['exam_subject_config'] or False
        exam_subject_types            = 'exam_subject_types' in data['form'] and data['form']['exam_subject_types'] or False



        grade_id                      = self.env['school.grade'].search([('id','=',grade_id[0])])
        educational_stage_id          = self.env['educational.stage'].search([('id','=',educational_stage_id[0])])
        class_id                      = self.env['school.class'].search([('id','=',class_id[0])])
        periode_ids                   = self.env['school.exam.periode'].search([('id','=',periode_ids[0])])
        academic_year                 = self.env['academic.year'].search([('id','=',academic_year[0])])
        periode_config_id             = self.env['school.exam.periode.config'].search([('id','=',periode_config_id[0])])
        subject_id                    = self.env['school.subject'].search([('id','=',subject_id[0])])
        student_id                    = self.env['school.student'].search([('class_id','=',class_id.id)])
        print "########## STUDENT ",student_id
        print "##### render_html 2"
        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            'docs': student_id,
            'time': time,
            'periode_id': periode_ids,
            'grade_id': grade_id,
            'subject_id':subject_id,
            'academic_year':academic_year,
            'educational_stage_id':educational_stage_id,
            'periode_config_id':periode_config_id,
            'class_id':class_id,
            'exam_types': self._get_exam_types,
            'subject_note': self._get_subject_note,
            'subject_average': self._get_subject_average,
        }
        report = self.env['report'].render('school_exams.report_schoolexamnotes', docargs)
        print "############# hello"
        return report
