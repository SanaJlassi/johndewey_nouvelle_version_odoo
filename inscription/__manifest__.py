{

    'name': 'Inscription',
    'summary' : "",
    'version' : "1.0",

    'author': 'Diginov Team',
    'website': 'https://diginov.tech/',
    'depends': ['school_config','point_of_sale'],

    'data': [
         'views/init_data.xml',
         'views/inscription_view.xml',


    ],

    'installable': True,
    'auto_install': False,

}