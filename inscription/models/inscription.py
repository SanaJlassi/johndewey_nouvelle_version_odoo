# -*- coding: utf-8 -*-

from datetime import timedelta, datetime
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError
from dateutil.relativedelta import relativedelta
from odoo.osv import osv



class pos_category(models.Model):
    _inherit = 'pos.category'

    price = fields.Float(string="Price")
    place_number = fields.Integer(string="place number")
    academic_year = fields.Many2one('academic.year', ondelete='cascade', string="Academic year")


    start_date_registration = fields.Date(string='Start Registration Date')
    stop_date_registration = fields.Date(string='End Registration Date')
    date_start = fields.Date(string='Start Date')
    date_stop = fields.Date(string='End Date')
    services_ids = fields.One2many('product.template', 'pos_categ_id', ondelete='cascade', string='Months')
    product_category = fields.Many2one('product.category', string="Category Type")
    # cash = fields.Boolean(default=False, string="Cash")
    school_product_type = fields.Selection([('registration', 'Registration' ),
                                            ('event', 'Event'),('study', 'Study'),
                                            ('extra', 'Extra')])
    groups = fields.Many2one('school.grade', 'Groupe', ondelete='cascade')
    excluded_services_ids = fields.Many2many('pos.category', 'service_category_rel', 'category_id', 'service_id', string='Excluded services')
    method_rec = fields.Selection(related='academic_year.method')
    # term_rec = fields.One2many(related='academic_year.trimest')
    # semestre_rec = fields.One2many(related='academic_year.semestre')
    @api.onchange('academic_year')
    def _set_start_stop_date(self):
        self.date_start = self.academic_year.start_date
        self.date_stop = self.academic_year.end_date

    _sql_constraints = [
        ('check_date',
         'CHECK(date_start <= date_stop)',
         "Start Date Must be Below End Date"),
        ('check_registration_date',
         'CHECK(start_date_registration < stop_date_registration)',
         "Start Date Registration Must be Below End Date registration"),
        ('check_registration_date_stop_date',
         'CHECK(stop_date_registration <= date_stop)',
         "Stop Date Registration Must be Below End Date")
    ]


    def generate_months(self):
        period_obj = self.env['product.template']

        ds = datetime.strptime(self.date_start, '%Y-%m-%d')
        while ds.strftime('%Y-%m-%d') < self.date_stop:
                de = ds + relativedelta(months=1, days=-1)
                if de.strftime('%Y-%m-%d') > self.date_stop:
                    de = datetime.strptime(self.date_stop, '%Y-%m-%d')

                period_obj.create({
                    'name': "%s %s" % (self.name, ds.strftime('%m/%Y')),
                    'pos_categ_id': self.id,
                    'type': 'service',
                    'available_in_pos': True,
                    'list_price': self.price,
                    'subscription_month': ds.strftime('%m/%Y'),
                    'school_product_type': self.school_product_type,
                    'academic_year': self.academic_year.id,
                    'categ_id': self.product_category.id,
                    #'cash': self.cash,
                })
                ds = ds + relativedelta(months=1)
        return True

    def generate_inscriptions(self):
        property_account_income = self.env['account.account'].search([('code', '=', '707000')])
        print property_account_income
        period_obj = self.env['product.template']
        sequence = 1

        if (self.academic_year.state) == "current" or (self.academic_year.state) == "closed":
            error = "You cannot generate inscriptions year that have '%s' status" % _(self.academic_year.state)
            raise ValidationError(error)

        period_obj.create({
            'name': "%s %s %s" % (_("Inscription"), self.academic_year.code,self.groups.name),
            'pos_categ_id': self.id,
            'type': 'service',
            'available_in_pos': True,
            'property_account_income_id': property_account_income.id,
            'school_product_type': self.school_product_type,
            'academic_year': self.academic_year.id,
            'categ_id': self.product_category.id,
            'inscription_sequence': sequence,
            'group_id':self.groups.id,
        })
        sequence = sequence + 1
        period_obj.create({
            'name': "%s %s %s" % (_("Réinscription"), self.academic_year.code,self.groups.name),
            'pos_categ_id': self.id,
            'type': 'service',
            'available_in_pos': True,
            'property_account_income_id': property_account_income.id,
            'school_product_type': self.school_product_type,
            'academic_year': self.academic_year.id,
            'categ_id': self.product_category.id,
            'inscription_sequence': sequence,
            'group_id':self.groups.id,
        })
        return True

    @api.multi
    def generate_months_study(self):
        property_account_income = self.env['account.account'].search([('code', '=', '707000')])


        if (self.academic_year.method == 'mensuel'):
            period_obj = self.env['product.template']
            ds = datetime.strptime(self.academic_year.start_date, '%Y-%m-%d')
            while ds.strftime('%Y-%m-%d') < self.academic_year.end_date:
                de = ds + relativedelta(months=1, days=-1)
                if de.strftime('%Y-%m-%d') > self.academic_year.end_date:
                    de = datetime.strptime(self.academic_year.end_date, '%Y-%m-%d')

                period_obj.create({
                    'name': "%s %s" % (self.name, ds.strftime('%m/%Y')),
                    'pos_categ_id': self.id,
                    'type': 'service',
                    'available_in_pos': True,
                    'list_price': self.price,
                    'property_account_income_id': property_account_income.id,
                    'subscription_month': ds.strftime('%m/%Y'),
                    'school_product_type': self.school_product_type,
                    'academic_year': self.academic_year.id,
                    #'cash': self.cash,
                    'categ_id': self.product_category.id,

                })
                ds = ds + relativedelta(months=1)
        if (self.academic_year.method == 'semestriel'):
            period_obj = self.env['product.template']

            count_semestre = len(self.academic_year.semestre)
            i=0
            while (i < count_semestre):
                nb = len(self.academic_year.semestre[i].mois)
                mois_id = self.academic_year.semestre[i].mois[nb-1]
                period_obj.create({
                    'name': "%s %s " % (self.name, (self.academic_year.semestre)[i].code),
                    'pos_categ_id': self.id,
                    'type': 'service',
                    'available_in_pos': True,
                    'list_price': self.price,
                    'property_account_income_id': property_account_income.id,
                    'subscription_month': mois_id.code,
                    'school_product_type': self.school_product_type,
                    'academic_year': self.academic_year.id,
                    #'cash': self.cash,
                    'categ_id': self.product_category.id,

                })
                i+=1

        if (self.academic_year.method == 'trimestriel'):
            period_obj = self.env['product.template']

            count_trimestre = len(self.academic_year.trimest)
            i = 0
            while (i < count_trimestre):
                nb = len(self.academic_year.trimest[i].mois)
                mois_id = self.academic_year.trimest[i].mois[nb - 1]
                period_obj.create({
                    'name': "%s %s " % (self.name, (self.academic_year.trimest)[i].code),
                    'pos_categ_id': self.id,
                    'type': 'service',
                    'available_in_pos': True,
                    'list_price': self.price,
                    'property_account_income_id': property_account_income.id,
                    'subscription_month': mois_id.code,
                    'school_product_type': self.school_product_type,
                    'academic_year': self.academic_year.id,
                    #'cash': self.cash,
                    'categ_id': self.product_category.id,

                })
                i += 1

        return True

    # def generate_months_service(self, cr, uid, ids, context=None, interval=1):
    #     property_account_income = self.pool.get('account.account').search(cr, uid, [('code', '=', '7050002')])
    #     period_obj = self.pool.get('product.template')
    #     for fy in self.browse(cr, uid, ids, context=context):
    #         ds = datetime.strptime(fy.date_start, '%Y-%m-%d')
    #         while ds.strftime('%Y-%m-%d') < fy.date_stop:
    #             de = ds + relativedelta(months=interval, days=-1)
    #             if de.strftime('%Y-%m-%d') > fy.date_stop:
    #                 de = datetime.strptime(fy.date_stop, '%Y-%m-%d')
    #
    #             period_obj.create(cr, uid, {
    #                 'name': "%s %s" % (fy.name, ds.strftime('%m/%Y')),
    #                 'pos_categ_id': fy.id,
    #                 'type': 'service',
    #                 'available_in_pos': True,
    #                 'list_price': fy.price,
    #                 'property_account_income': property_account_income[0],
    #                 'subscription_month': ds.strftime('%m/%Y'),
    #                 'school_product_type': fy.school_product_type,
    #                 'categ_id': fy.product_category.id,
    #                 'academic_year': fy.academic_year.id,
    #                 'cash': fy.cash,
    #             })
    #             ds = ds + relativedelta(months=interval)
    #     return True


class product_template(models.Model):
    _inherit = 'product.template'


    subscription_month = fields.Char(string='Subscription Month')
    #cash = fields.Boolean(default=False, string="Cash")
    school_product_type = fields.Selection(
        [('service', 'Service'), ('registration', 'Registration'), ('event', 'Event'),
         ('study', 'Study'), ('extra', 'Extra')])
    inscription_sequence = fields.Integer(string="sequence")
    academic_year = fields.Many2one('academic.year', ondelete='cascade', string="Academic year")
    group_id = fields.Many2one('school.grade', 'Groupe', ondelete='cascade')


    @api.model
    def create(self, vals):

        property_account_income = self.env['account.account'].search([('code', '=', '707000')])
        property_account_expense = self.env['account.account'].search([('code', '=', '607000')])

        res = super(product_template, self).create(vals)
        res.write({'property_account_income_id': property_account_income.id,
                   'property_account_expense_id':property_account_expense.id,})
        return  res


    #_sql_constraints = [
      #  ('unique_oschool_product', 'unique(academic_year, school_product_type, subscription_month, pos_categ_id)',
       #   'The record must be unique by year'),
        # ('unique_oschool_inscription_by_year',
        # 'unique(academic_year, school_product_type, inscription_sequence, categ_id)',
         #'The registration must be unique by year')]





