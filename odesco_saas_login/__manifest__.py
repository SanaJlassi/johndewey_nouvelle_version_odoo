# -*- coding: utf-8 -*-
{
    'name': 'Odesco saas Login API',
    'version': '1.0.0',
    'author': 'Ben Selma Sghaier',
    'license': 'LGPL-3',
    'category': 'SaaS',
    'depends': [
        'base'
    ],
    'data': [
        'views/res_users_view.xml',
    ],
    'installable': True,
}
