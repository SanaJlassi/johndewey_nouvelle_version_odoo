# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import odoo.addons.decimal_precision as dp
from odoo.tools.amount_to_text import amount_to_text_fr


MONTHS = ['Janvier','Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre']


class FeesConfig(models.Model):
    _name = 'fees.config'


    name                = fields.Char(string='Titre de configuration', required=True)
    academic_year_id    = fields.Many2one('academic.year', string='Année scolaire', required=True)
    grade_id            = fields.Many2one('school.grade', string='Niveau scolaire', required=True)
    config_line_ids     = fields.One2many('fees.config.line', 'fees_periode_config_id', string='Liste des fairs')




class FeesConfigLine(models.Model):
    _name = 'fees.config.line'


    name                    = fields.Char(string='Name')
    fees_periode_id         = fields.Many2one('fees.period.config', string='Période', required=True)
    fees_id                 = fields.Many2one('fees.fees', string='Frais', required=True)
    price                   = fields.Float(string='Montant', required=True, digits=0)
    fees_periode_config_id  = fields.Many2one('fees.config', string='Cofig', required=True)




class FeesPeriodeConfig(models.Model):
    _name = 'fees.period.config'
    _order = 'order asc'

    name                = fields.Char(string="Titre de période")
    date_begin          = fields.Date(string="Date début", required=True)
    date_end            = fields.Date(string="Date fin", required=True)
    order               = fields.Integer(string="Order d'affichage")

    _sql_constraints = [('field_unique', 'unique(order)','Choose another value - it has to be unique!')]


    @api.one
    @api.constrains('date_begin', 'date_end')  
    def _check_dates(self): 
        date_begin = datetime.strptime(self.date_begin, DEFAULT_SERVER_DATE_FORMAT)  
        date_end = datetime.strptime(self.date_end, DEFAULT_SERVER_DATE_FORMAT)  
        if date_begin > date_end:
            raise ValidationError("La date de début de période doit être inférieure à la date de fin de période.")


    @api.onchange('date_begin', 'date_end')
    def _change_dates(self):
        print "######11 DATE BEGIN ",self.date_begin
        print "###### 1 DATE ENND ",self.date_end
        if self.date_begin and self.date_end:
            date_begin = datetime.strptime(self.date_begin, DEFAULT_SERVER_DATE_FORMAT)  
            date_end = datetime.strptime(self.date_end, DEFAULT_SERVER_DATE_FORMAT)  
            if date_begin >= date_end:
                return {
                    'warning': {'title': "Date de période", 'message': "La date de début de période doit être inférieure à la date de fin de période."},
                }


    @api.model
    def create(self, vals):
        print "### VALS ",vals
        if not vals.get('name',False):
            begin_month_number = datetime.strptime(vals.get('date_begin',False), DEFAULT_SERVER_DATE_FORMAT).month
            begin_year_number = str(datetime.strptime(vals.get('date_begin',False), DEFAULT_SERVER_DATE_FORMAT).year)

            end_month_number = datetime.strptime(vals.get('date_begin',False), DEFAULT_SERVER_DATE_FORMAT).month
            end_year_number = str(datetime.strptime(vals.get('date_begin',False), DEFAULT_SERVER_DATE_FORMAT).year)

            vals['name'] = MONTHS[begin_month_number][:3]+' '+begin_year_number[2:]+' - '+MONTHS[end_month_number][:3]+' '+end_year_number[2:]
        return super(FeesPeriodeConfig, self).create(vals)


    @api.multi
    def write(self, vals):
        print "##### VALS write ",vals
        return super(FeesPeriodeConfig, self).create(vals)



class FeesPeriode(models.Model):
    _name = 'fees.fees'



    name                = fields.Char(string='Titre de frais', require=True)
    active              = fields.Boolean(string="Active", default=True)
    

    @api.model
    def create(self, vals):
        print "### VALS ",vals
        return super(FeesPeriode, self).create(vals)


    @api.multi
    def write(self, vals):
        print "##### VALS write ",vals
        return super(FeesPeriode, self).create(vals)
    