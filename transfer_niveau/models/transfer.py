# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo import _
from odoo.exceptions import UserError, ValidationError

class TransfertWizard(models.TransientModel):
    _name = 'transfer.wizard'

    @api.multi
    @api.depends('year','level')
    def _default_count(self):



        list = self.env['pos.order.line'].search(['&','&',('product_id.school_product_type','=','registration'),
                                                  ('academic_year_id', '=', self.year.id),
                                                  ('group_id','=', self.level.id)])
        self.count_student = len(list)




    year = fields.Many2one('academic.year', string="Current year", domain=[('state', '=', 'current')])
    level = fields.Many2one('school.grade',string='Current level')
    next_year = fields.Many2one('academic.year', string="Next year", domain=[('state', '=', 'new')])
    next_level = fields.Many2one('school.grade', string='Next level')
    check_level = fields.Integer(related='level.check', store=True)
    group = fields.Many2one('educational.stage',readonly=True,related='level.educational_stage_id',string="Groupe", store=True)
    count_student = fields.Integer(string="Nombre d'élèves", compute='_default_count', readonly=True, store=True)
    product_id = fields.Many2one('product.product', string='Inscription')
    next_class = fields.Many2one('school.class', 'Class')

    @api.multi
    def button_save(self):

        list = self.env['pos.order.line'].search(['&', '&', ('product_id.school_product_type', '=', 'registration'),
                                                  ('academic_year_id', '=', self.year.id),
                                                  ('group_id', '=', self.level.id)])
        print list
        if len(list) == 0:
            raise UserError(_("le nombre d'élève égale à 0"))

        else:
            list_student = self.env['school.student'].search([('group_id', '=', self.next_level.id)])
            print list_student
            if list_student:
                raise UserError('level already transfered')

            for l in list :


                res= self.env['school.student'].create({'name':l.student_id.name,
                                                    'last_name':l.student_id.last_name,
                                                    'legal_responsable':l.student_id.legal_responsable.id ,
                                                    'pid':l.student_id.pid,
                                                    'partner_id':l.student_id.partner_id.id,
                                                    'user_id':l.student_id.user_id.id
                                                  })



                product = self.env['product.product'].browse(self.product_id.id)

                self.env['pos.order.line'].create({ 'student_id': res.id,
                                                    'parent_id': res.legal_responsable.id,
                                                    'product_id':self.product_id.id,
                                                    'type': 'registration',
                                                    'group_id': self.next_level.id,
                                                    'class_id': self.next_class.id,
                                                    'academic_year_id': self.next_year.id,
                                                    'registration_price': product.product_tmpl_id.list_price,
                                                    'price_unit': product.product_tmpl_id.list_price
                                                  })



            return res


