# -*- coding: utf-8 -*-

{
    'name' : "transfer",
    'summary': '',

    'description': """
    Transfer level module. Test module to try things
    
""",
   'author':"Diginov",
    'website':"https://www.linkedin.com/in/sirine/",
    'category':'Training',
    'version':'1.0',
    'depends': ['base', 'genext_school','registration'],
    #'board', 'mail', 
    'data':[

        "views/transfer.xml",

    ],
    'demo':[],
    'application':True,
    'sequence': 1,
}