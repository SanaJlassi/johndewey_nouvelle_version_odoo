# -*- coding: utf-8 -*-


from odoo.osv import osv
from odoo import models, api, fields, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp
from odoo.tools.translate import _


class Student(models.Model):
    _inherit='school.student'

    extra_ids = fields.One2many('pos.order.line', 'student_id', 'Extra List',
                                domain=[('type', '=', 'extra'), ('state_academic_year', '!=', 'closed')])

    study_ids = fields.One2many('pos.order.line', 'student_id', 'Study List',
                                domain=[('type', '=', 'study'), ('state_academic_year', '!=', 'closed')])


    @api.multi
    def study_student(self):
        category_obj = self.env['pos.category']
        pos_line_obj = self.env['pos.order.line']
        period_obj = self.env['account.period']
        for student in self.browse(self.ids):
            
            if student.academic_year.state == 'new' :
                raise UserError(
                    _("impossible de générer les études lorsque l'état de l'année est nouveau"))

            if student.academic_year.state == 'closed' :
                raise UserError(
                    _("impossible de générer les études lorsque l'état de l'année est fermé"))

            study_id = category_obj.search(['&', ('school_product_type', '=', 'study'), ('academic_year', '=', student.academic_year.id), ('groups', '=', student.group_id.id)])
            if not study_id:
                raise UserError(_("Il n'y a pas d'études définies pour l'année scolaire: %s.") % (student.academic_year.name))

            #registration_id = pos_line_obj.search(['&', ('student_id', '=', student.id), ('type', '=', 'registration'), ('academic_year_id', '=', student.academic_year.id), ('qty', '!=', -1)])
            #registration = pos_line_obj.browse(registration_id)

            #if not registration_id.order_id or (registration_id.order_id and registration_id.order_id.state == 'draft'):
                #raise UserError(_('There is no registration paid for this academic year: %s.') % student.academic_year.name)

            if len(pos_line_obj.search(['&', ('student_id', '=', student.id), ('type', '=', 'study'), ('academic_year_id', '=', student.academic_year.id)])) > 0:
                raise UserError(_('Study already exist for %s.') % student.academic_year.name)

            if not student.class_id:
                raise UserError(_('Class of student missing'))

            for study in category_obj.browse(study_id.id).services_ids:
                if period_obj.browse(period_obj.search([('code', '=', study.subscription_month)]).id).state == 'done':
                    continue
                if not period_obj.search([('code', '=', study.subscription_month)]):
                    raise UserError(_('period %s not exist.') % study.subscription_month)

                vals = { 'product_id': study.product_variant_ids[0].id,
                         'student_id': student.id,
                         'parent_id': student.legal_responsable.id,
                         'type': 'study',
                         'price_unit':study.list_price,
                         'qty': 1.0,
                         'academic_year_id': student.academic_year.id,
                         'period_id': period_obj.search([('code', '=', study.subscription_month)]).id,
                       }
                # inv = pos_line_obj.onchange_product_id(study.id, student.parent_id.property_product_pricelist.id, study.product_variant_ids[0].id, 1, student.parent_id.id)
                # inv['value']['product_id'] = study.product_variant_ids[0].id
                # inv['value']['student_id'] = student.id
                # inv['value']['parent_id'] = student.parent_id.id
                # inv['value']['type'] = 'study'
                # inv['value']['qty'] = 1.0
                # inv['value']['academic_year_id'] = student.academic_year_id.id
                # inv['value']['period_id'] = period_obj.search(cr, uid, [('code', '=', study.subscription_month)])[0]
                pos_line_obj.create(vals)
        return True




    @api.multi
    def extra_student(self):
        category_obj = self.env['pos.category']
        pos_line_obj = self.env['pos.order.line']
        period_obj = self.env['account.period']
        for student in self.browse(self.ids):
            
            if student.academic_year.state == 'new' :
                raise UserError(
                    _("impossible de générer les études lorsque l'état de l'année est nouveau"))

            if student.academic_year.state == 'closed' :
                raise UserError(
                    _("impossible de générer les études lorsque l'état de l'année est fermé"))           

            study_id = category_obj.search(
                ['&', ('school_product_type', '=', 'extra'), ('academic_year', '=', student.academic_year.id),
                 ('groups', '=', student.group_id.id)])
            if not study_id:
                raise UserError(
                    _("Il n'y a pas d'extras définies pour l'année scolaire: %s.") % (student.academic_year.name))

            # registration_id = pos_line_obj.search(['&', ('student_id', '=', student.id), ('type', '=', 'registration'),
            #                                        ('academic_year_id', '=', student.academic_year.id),
            #                                        ('qty', '!=', -1)])
            # registration = pos_line_obj.browse(registration_id)

            # if not registration_id.order_id or (registration_id.order_id and registration_id.order_id.state == 'draft'):
            #     raise UserError(
            #         _('There is no registration paid for this academic year: %s.') % student.academic_year.name)
            #
            # if len(pos_line_obj.search(['&', ('student_id', '=', student.id), ('type', '=', 'extra'),
            #                             ('academic_year_id', '=', student.academic_year.id)])) > 0:
            #     raise UserError(_('Extra already exist for %s.') % student.academic_year.name)

            if not student.class_id:
                raise UserError(_('Class of student missing'))

            for  cat in study_id:

                 for study in category_obj.browse(cat.id).services_ids:



                     if period_obj.browse(period_obj.search([('code', '=', study.subscription_month)]).id).state == 'done':
                        continue
                     if not period_obj.search([('code', '=', study.subscription_month)]):
                        raise UserError(_('period %s not exist.') % study.subscription_month)



                     vals = {'product_id': study.product_variant_ids[0].id,
                            'student_id': student.id,
                            'parent_id': student.legal_responsable.id,
                            'type': 'extra',
                            'price_unit': study.list_price,
                            'qty': 1.0,
                            'academic_year_id': student.academic_year.id,
                            'period_id': period_obj.search([('code', '=', study.subscription_month)]).id,
                                }

                     pos_line_obj.create(vals)
        return True




    @api.one
    @api.depends('product_id')
    def _compute_study_price(self):

        self.study_price = self.product_id.product_tmpl_id.list_price

# class pos_order_line(models.Model):
#     _inherit = 'pos.order.line'
#
#     def study_student(self, cr, uid, ids, context=None):
#         if not ids:
#             return []
#         dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'oschool', 'view_oschool_study_wizard')
#         inv = self.browse(cr, uid, ids[0], context=context)
#         if inv.order_id:
#             raise osv.except_osv(_('Warning!'), _('Please select another line because it have a order.'))
#         if inv.period_id.state != 'draft':
#             raise osv.except_osv(_('Warning!'), _('Please select another line because period is closed.'))
#         return {
#             'name': _("Update Study"),
#             'view_mode': 'form',
#             'view_id': view_id,
#             'view_type': 'form',
#             'res_model': 'oschool.study.update',
#             'type': 'ir.actions.act_window',
#             'nodestroy': True,
#             'target': 'new',
#             'domain': '[]',
#             'context': {
#                 'default_subscriber': inv.subscriber,
#             }
#         }

#     def study_refund(self, cr, uid, ids, context=None):
#         if not ids:
#             return []
#         clone_list = []
#         inv = self.browse(cr, uid, ids[0], context=context)
#         if not inv.order_id:
#             raise osv.except_osv(_('Warning!'), _('Please select another line because it not have a order.'))
#         if not inv.subscriber:
#             raise osv.except_osv(_('Warning!'), _('Please select another line because it not subscriber.'))
#         if inv.order_id and inv.order_id.state == 'draft':
#             raise osv.except_osv(_('Warning!'), _('Please select another line because it not payed.'))
#         if inv.refunded:
#             raise osv.except_osv(_('Warning!'), _('Please select another line because it Refunded.'))
#         if inv.qty < 0:
#             clone_list.append(inv.order_id.id)
#         else:
#             line_obj = self.pool.get('pos.order.line')
#
#             for order in self.browse(cr, uid, ids, context=context).order_id:
#                 current_session_ids = self.pool.get('pos.session').search(cr, uid, [
#                 ('state', '!=', 'closed'),
#                 ('user_id', '=', uid)], context=context)
#                 if not current_session_ids:
#                     raise osv.except_osv(_('Error!'), _('To return product(s), you need to open a session that will be used to register the refund.'))
#
#                 clone_id = self.pool.get('pos.order').copy(cr, uid, order.id, {
#                 'name': order.name + ' REFUND', # not used, name forced by create
#                 'session_id': current_session_ids[0],
#                 'date_order': time.strftime('%Y-%m-%d %H:%M:%S'),
#             }, context=context)
#                 clone_list.append(clone_id)
#
#             for clone in self.pool.get('pos.order').browse(cr, uid, clone_list, context=context):
#                 for order_line in clone.lines:
#                     line_obj.write(cr, uid, [order_line.id], {
#                     'qty': -order_line.qty
#                 }, context=context)
#                     if order_line.type != 'study':
#                         line_obj.unlink(cr, uid, order_line.id)
#             line_obj.write(cr, uid, inv.id, {'refunded': True}, context=context)
#         dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'oschool', 'view_oschool_study_pos_form')
#
#         return {
#             'name': _("Refund Study"),
#             'view_type': 'form',
#             'view_mode': 'form',
#             'res_id': clone_list[0],
#             'view_id': view_id,
#             'view_type': 'form',
#             'res_model': 'pos.order',
#             'type': 'ir.actions.act_window',
#             'nodestroy': True,
#             'target': 'new',
#             'nodestroy': True,
#             'context': {
#                 'subscription_month': inv.period_id.code,
#             }
#         }
#
# pos_order_line()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
