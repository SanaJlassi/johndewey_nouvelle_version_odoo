# -*- coding: utf-8 -*-


from odoo.osv import osv
from odoo import models, api, fields,_
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp
from odoo.tools.translate import _



class study_student(models.TransientModel):
    _name = 'study.student'

    category_obj = fields.Many2one('pos.category', string='Scolarité', required=True)
    group_id = fields.Many2one(string='Niveau scolaire',related='category_obj.groups', readonly=True, store=True )
    price = fields.Float(string='Prix', related='category_obj.price', readonly=True, store=True )
    state = fields.Selection([('open', 'Ouvert'), ('close', 'Validé')], 'Status',
                             readonly=True, copy=False, default='open')



    @api.multi
    def study_student(self):

        students = []
        category_obj = self.env['pos.category']
        pos_line_obj = self.env['pos.order.line']
        period_obj = self.env['account.period']

        students = self.env['school.student'].search([('group_id', '=', self.group_id.id)])

        for student in students:
            
            if student.academic_year.state == 'new' :
                raise UserError(
                    _("impossible de générer les études lorsque l'état de l'année est nouveau"))

            if student.academic_year.state == 'closed' :
                raise UserError(
                    _("impossible de générer les études lorsque l'état de l'année est fermé"))

        
            study_id = category_obj.search(
                ['&', ('school_product_type', '=', 'study'), ('academic_year', '=', student.academic_year.id),
                 ('groups', '=', student.group_id.id)])
            if not study_id:
                raise UserError(_("Il n'y a pas d'études définies  pour l'année scolaire: %s") % (student.academic_year.name))
            registration_id = pos_line_obj.search(['&', ('student_id', '=', student.id), ('type', '=', 'registration'),
                                                   ('academic_year_id', '=', student.academic_year.id),
                                                   ('qty', '!=', -1)])
            # registration = pos_line_obj.browse(registration_id)
            if not registration_id.order_id or (registration_id.order_id and registration_id.order_id.state == 'draft'):
                raise UserError(_( 'There is no registration paid for the student %s %s for this academic year: %s.') %
                                               (student.name,student.last_name,student.academic_year.name))


            if len(pos_line_obj.search(['&','&', ('student_id', '=', student.id), ('type', '=', 'study'), ('academic_year_id', '=', student.academic_year.id)])) > 0:
                raise UserError(_('Study already exist for %s.') % student.academic_year.name)


            if not student.class_id:
                raise UserError(_('Class of student missing'))
            for study in category_obj.browse(study_id.id).services_ids:
                if period_obj.browse(period_obj.search([('code', '=', study.subscription_month)]).id).state == 'done':
                    continue
                if not period_obj.search([('code', '=', study.subscription_month)]):
                    raise UserError(_('period %s not exist.') % study.subscription_month)

                vals = {'product_id': study.product_variant_ids[0].id,
                        'student_id': student.id,
                        'parent_id': student.legal_responsable.id,
                        'type': 'study',
                        'price_unit': study.list_price,
                        'qty': 1.0,
                        'academic_year_id': student.academic_year.id,
                        'period_id': period_obj.search([('code', '=', study.subscription_month)]).id,
                        }
                # inv = pos_line_obj.onchange_product_id(study.id, student.parent_id.property_product_pricelist.id, study.product_variant_ids[0].id, 1, student.parent_id.id)
                # inv['value']['product_id'] = study.product_variant_ids[0].id
                # inv['value']['student_id'] = student.id
                # inv['value']['parent_id'] = student.parent_id.id
                # inv['value']['type'] = 'study'
                # inv['value']['qty'] = 1.0
                # inv['value']['academic_year_id'] = student.academic_year_id.id
                # inv['value']['period_id'] = period_obj.search(cr, uid, [('code', '=', study.subscription_month)])[0]

                if len(pos_line_obj.search(['&', ('student_id', '=', student.id), ('type', '=', 'study'),
                                            ('academic_year_id', '=', student.academic_year.id)])) > 0:
                    pos_line_obj.write(vals)

                pos_line_obj.create(vals)
                self.write({'state':'close'})
        return True

