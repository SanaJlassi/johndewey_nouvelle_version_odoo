# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

from odoo.exceptions import UserError
from odoo.tools.translate import _, _logger


class oschool_payment(models.TransientModel):
    _name = 'odesco.payment'


    @api.onchange('period_id')
    def _onchange_period(self):


        if self.student_id and self.period_id:
            line_idss = self.env['pos.order.line'].search(['&','&','&', ('type', '!=', ['registration','event']), ('period_id', '=', self.period_id.id),
                                                          ('student_id', '=', self.student_id.id),('order_id', '=', False)])
            self.line_ids = line_idss

    @api.onchange('trimestre')
    def _onchange_trimstre(self):

        if self.student_id and self.trimestre:

            line_idss = self.env['pos.order.line'].search(
                ['&', '&', '&',  ('type', '!=', ['registration','event']), ('period_id', 'in', self.trimestre.mois.ids),
                 ('student_id', '=', self.student_id.id), ('order_id', '=', False)])

            self.line_ids = line_idss

    @api.onchange('semestre')
    def _onchange_semestre(self):

        if self.student_id and self.semestre:

            line_idss = self.env['pos.order.line'].search(
                ['&', '&', '&',  ('type', '!=', ['registration','event']), ('period_id', 'in', self.semestre.mois.ids),
                 ('student_id', '=', self.student_id.id), ('order_id', '=', False)])

            self.line_ids = line_idss

    @api.onchange('student_id')
    def _onchange_student(self):

        if self.student_id and self.period_id or self.trimestre or self.semestre:
            line_idss = self.env['pos.order.line'].search(
                ['&', '&', '&', ('type', '!=', ['registration','event']), ('period_id', '=', self.period_id.id),
                 ('student_id', '=', self.student_id.id),  ('order_id', '=', False)])

            self.line_ids = line_idss

    @api.multi
    def button_payment(self):
        pos_ref = self.env['pos.order']
        pos_line_ref = self.env['pos.order.line']
        journal_obj = self.env['account.journal']

        for line in self.browse(self.ids):
            if line.trimestre:
                l = pos_line_ref.search(['&','&','&', ('type', '!=', ['registration','event']), ('period_id', 'in', line.trimestre.mois.ids),
                                                          ('student_id', '=', line.student_id.id),('order_id', '=', False)])

            if line.semestre:
                l = pos_line_ref.search(['&', '&', '&',  ('type', '!=', ['registration', 'event']),
                                         ('period_id', 'in', line.semestre.mois.ids),
                                         ('student_id', '=', line.student_id.id),
                                         ('order_id', '=', False)])


            if line.period_id:
                l = pos_line_ref.search(['&', '&', '&',  ('type', '!=', ['registration', 'event']),
                                         ('period_id', '=', line.period_id.id),
                                         ('student_id', '=', line.student_id.id),
                                         ('order_id', '=', False)])



            if not l:
                raise UserError(_('Pas de résultat.'))
            pos = ''

            if line.period_id:
                self._cr.execute("""select distinct ap.id from pos_order_line pol inner join account_period ap on ap.id = pol.period_id
                                  left outer join pos_order po on po.id = pol.order_id
                                  where pol.type != 'registration' and pol.student_id = %s and ap.date_start < %s
                                  and (pol.order_id is null or po.state = 'draft')""",
                                 (line.student_id.id, line.period_id.date_start))

                for id in self._cr.fetchall():
                    pos += self.env['account.period'].browse(id).name + ' ,'
                if pos:
                    raise UserError(_('period %s is not paid' % pos))

            for i in l :
                if pos_line_ref.browse(i.id).order_id:
                    inv_id = pos_line_ref.browse(i.id).order_id.id
                else:
                    inv = {'partner_id': line.responsible_id.id,
                       'student_id': line.student_id.id,
                       'pricelist_id':1,}
                    inv_id = pos_ref.create(inv)
                    l.write({'order_id': inv_id.id})


                res = self.env.ref('registration.view_odesco_registration_pos_form')

                return {
                'name': _('Payement'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'pos.order',
                'type': 'ir.actions.act_window',
                'domain': [('id', '=', inv_id.id)],
                'view_id': res.id,
                'res_id': inv_id.id,
                    }


    responsible_id = fields.Many2one('res.partner', 'Responsible')
    student_id = fields.Many2one('school.student', 'Student', domain="[('legal_responsable','=', responsible_id)]")
    academic_year_id = fields.Many2one('academic.year', 'Academic year')
    method = fields.Selection(related='academic_year_id.method', string='Méthode de payement', store=True)
    trimestre = fields.Many2one('trimestre_method', string='Trimestre')
    semestre = fields.Many2one('semestre_method', string='Semestre')
    period_id = fields.Many2one('account.period', 'Period')
    line_ids = fields.Many2many('pos.order.line', 'payment_line_rel', 'payment_id', 'line_id', string="Lines")
    #cash = fields.Boolean("Cash")




