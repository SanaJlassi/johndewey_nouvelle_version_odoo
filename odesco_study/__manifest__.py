{

    'name': 'Study',
    'summary' : "",
    'version' : "1.0",

    'author': 'Diginov Team',
    'website': 'https://diginov.tech/',
    'depends': ['school_config','point_of_sale','registration','genext_school'],

    'data': [

         'views/odesco_study_view.xml',
         'views/odesco_study_student_view.xml',
         'views/odesco_generate_study.xml',
         'views/odesco_extra_view.xml',
         'wizard/odesco_responsible_payment_view.xml',



    ],

    'installable': True,
    'auto_install': False,

}