# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class SchoolStudent(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }

    @http.route('/api/parent/student/<int:student_id>/attendance', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_attendance_by_student_parent(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)     


        class_id = student_id.class_id
        print "#### CLASS ID ",class_id
        academic_year_id = request.env['academic.year'].search([('active_year','=',True)])
        attendance_id = request.env['school.attendance'].search(['&',('class_id','=',class_id.id),('academic_year','=',academic_year_id.id)])
        print "#### ATTENDANCE ",attendance_id
        att_list = []
        for att in attendance_id:
            for line in att.attendance_line_ids:
                if line.student_id.id == student_id.id :
                    att_list.append({
                        'id':att.id,
                        'teacher':{
                            'id':att.teacher_id.id,
                            'name':att.teacher_id.name,
                            'last_name':att.teacher_id.last_name,
                        },
                        'subject':{
                            'id':att.subject_id.id,
                            'name':att.subject_id.name,
                            'code':att.subject_id.code,
                        },
                        'session':{
                            'start_time':att.session_id.start_time,
                            'end_time':att.session_id.end_time,
                            'dow':att.session_id.day_of_week,
                        },
                        'motif':line.motif,
                        'duree':line.date_time,
                        'date':att.datetime,
                        'state':line.state,
                    })
                    break
            
        print "#### DONE "
        response['success'] = True
        response['data'] = att_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)




    @http.route('/api/parent/student/<int:student_id>/attendance/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_attendance_by_student_academic_year_period(self, student_id,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}

        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)     


        class_id = student_id.class_id
        print "#### CLASS ID ",class_id
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        selected_academic_year = request.env['academic.year'].search([('id','=',academic_year_id)])
        attendance_id = request.env['school.attendance'].search(['&',('class_id','=',class_id.id),('academic_year','=',selected_academic_year.id)])
        print "#### ATTENDANCE ",attendance_id
        att_list = []
        for att in attendance_id:
            if timing_periode.start_date <= att.datetime <= timing_periode.end_date:
                for line in att.attendance_line_ids:
                    if line.student_id.id == student_id.id :
                        att_list.append({
                            'id':att.id,
                            'teacher':{
                                'id':att.teacher_id.id,
                                'name':att.teacher_id.name,
                                'last_name':att.teacher_id.last_name,
                            },
                            'subject':{
                                'id':att.subject_id.id,
                                'name':att.subject_id.name,
                                'code':att.subject_id.code,
                            },
                            'session':{
                                'start_time':att.session_id.start_time,
                                'end_time':att.session_id.end_time,
                                'dow':att.session_id.day_of_week,
                            },
                            'motif':line.motif,
                            'duree':line.date_time,
                            'date':att.datetime,
                            'state':line.state,
                        })
                        break
                
        print "#### DONE "
        response['success'] = True
        response['data'] = att_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)


    @http.route('/api/parent/student/<int:student_id>/subjects', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_subjects(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)     


        timetable_ids = request.env['time.table'].search(['&',('class_id','=',student_id.class_id.id),('academic_year','=',request.env['academic.year'].search([('active_year','=',True)]).id)])
        """                                                                             NEED TO BE FIXED CAN CONTAIN MULTI TIMETABLE                                                                              """
        for table in timetable_ids:
            print "####### table ",table.class_id.name
            print "####### table ",table.grade_id.name

        session_list = []
        for line in timetable_ids.time_table_session_ids:
            can_apend = True
            # for el in session_list:
            #     if line.class_id.id == el['class']['id']:
            #         can_apend = False
            #         break
            # if can_apend:
            session_list.append({
                'subject':{
                    'id':line.subject_id.id,
                    'name':line.subject_id.name,
                    'code':line.subject_id.code,
                },
                'teacher':{
                    'id':line.teacher_id.id,
                    'name':line.teacher_id.name,
                    'last_name':line.teacher_id.last_name
                }
            })




        # subject_ids =  student_id.class_id.grade_id.subject_ids

        # subject_list = []
        # for subject in subject_ids:
        #     subject_list.append({
        #         'id':subject.id,
        #         'name':subject.name,
        #         'code':subject.code,
        #     })
        response['success'] = True
        response['data'] = session_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)


    @http.route('/api/student/<int:student_id>/profile', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_children_profile(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
              
        res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
        if student_id not in res_partner.student_ids.ids:
            response['success'] = False
            response['error'] = {'code':404, 'message':'You need to be the parent of the student in order to get his info !'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        studnet_id = request.env['school.student'].search([('id','=',student_id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)            

        student = {
            'id':studnet_id.id,
            'pid':studnet_id.pid,
            'first_name':studnet_id.name,
            'last_name': studnet_id.last_name,
            'email':studnet_id.email,
            'phone':studnet_id.phone,
            'mobile':studnet_id.mobile,
            'image':studnet_id.image,
            'city':studnet_id.city,
            'zip_code':studnet_id.zip_code,
            'street_1':studnet_id.street_1,
            'street_2':studnet_id.street_2,
            'birth_date':studnet_id.birth_date,
            'age':studnet_id.age,
            'gender':studnet_id.gender,
            'state':studnet_id.state,
            'class':{
                'id':studnet_id.class_id.id,
                'name':studnet_id.class_id.name,
                'code':studnet_id.class_id.code,
            }
        }
            
        response['success'] = True
        response['data'] = student
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)



    @http.route('/api/student/<int:student_id>/tasks', type='http', auth="user", methods=['GET'],  csrf=False)
    def student_task(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
              
        res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
        if student_id not in res_partner.student_ids.ids:
            response['success'] = False
            response['error'] = {'code':404, 'message':'You need to be the parent of the student in order to get his info !'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        studnet_id = request.env['school.student'].search([('id','=',student_id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)  

        #tasks = request.env['school.task'].search([('state','=','approved')])
        tasks = request.env['school.task'].search([])
        task_list = []


        for task in sorted(tasks, key=lambda tasks: tasks.create_date, reverse=True):
            if studnet_id.id in task.student_ids.ids:
                files = []
                for file in task.file_ids:
                    domain = [
                        ('res_model', '=', file._name),
                        ('res_field', '=', 'file'),
                        ('res_id', '=', file.id),
                    ]
                    attachment = request.env['ir.attachment'].search(domain)
                    base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                    url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+file.file_name
                    url = url.replace(" ", "_")
                    _logger.warning('#### URL of document %s',url)
                    files.append({
                            'name':file.name,
                            'description':file.description,
                            'file_name':file.file_name,
                            'file':file.file,
                            'url':url,
                            'content_type':file.content_type,
                        })

                json_object = {
                    'id':task.id,
                    'name':task.name,
                    'creation_date':task.create_date,
                    'deadline':task.dead_line,
                    'description':task.description,
                    'all_student':task.all_student,
                    'grade':{
                        'id':task.grade_id.id,
                        'name':task.grade_id.name,
                    },
                    'class':{
                        'id':task.class_id.id,
                        'name':task.class_id.name,
                    },
                    'subject':{
                        'id':task.subject_id.id,
                        'name':task.subject_id.name,
                    },
                    'teacher':{
                        'id':task.teacher_id.id,
                        'name':task.teacher_id.name,
                    },
                    'periode':{
                        'id':task.timing_system_periode_id.id,
                        'name':task.timing_system_periode_id.name,
                    },
                    'files':files,
                }
                task_list.append(json_object)
        response['success'] = True
        response['data'] = task_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

    @http.route('/api/student/<int:student_id>/tasks/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def student_task_by_academic_year_period(self, student_id,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
              
        res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
        if student_id not in res_partner.student_ids.ids:
            response['success'] = False
            response['error'] = {'code':404, 'message':'You need to be the parent of the student in order to get his info !'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        studnet_id = request.env['school.student'].search([('id','=',student_id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)  
        selected_academic_year= request.env['academic.year'].search([('id','=',academic_year_id)])
        tasks = request.env['school.task'].search([('state','=','approved'),('timing_system_periode_id.id','=',timing_system_periode_id)])
        #tasks = request.env['school.task'].search([])
        task_list = []


        for task in sorted(tasks, key=lambda tasks: tasks.create_date, reverse=True):
            if  selected_academic_year.start_date <= task.creation_date <= selected_academic_year.end_date:
                if studnet_id.id in task.student_ids.ids:
                    files = []
                    for file in task.file_ids:
                        domain = [
                            ('res_model', '=', file._name),
                            ('res_field', '=', 'file'),
                            ('res_id', '=', file.id),
                        ]
                        attachment = request.env['ir.attachment'].search(domain)
                        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                        url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+file.file_name
                        url = url.replace(" ", "_")
                        _logger.warning('#### URL of document %s',url)
                        files.append({
                                'name':file.name,
                                'description':file.description,
                                'file_name':file.file_name,
                                'file':file.file,
                                'url':url,
                                'content_type':file.content_type,
                            })

                    json_object = {
                        'id':task.id,
                        'name':task.name,
                        'creation_date':task.create_date,
                        'deadline':task.dead_line,
                        'description':task.description,
                        'all_student':task.all_student,
                        'grade':{
                            'id':task.grade_id.id,
                            'name':task.grade_id.name,
                        },
                        'class':{
                            'id':task.class_id.id,
                            'name':task.class_id.name,
                        },
                        'subject':{
                            'id':task.subject_id.id,
                            'name':task.subject_id.name,
                        },
                        'teacher':{
                            'id':task.teacher_id.id,
                            'name':task.teacher_id.name,
                        },
                        'periode':{
                            'id':task.timing_system_periode_id.id,
                            'name':task.timing_system_periode_id.name,
                        },
                        'files':files,
                    }
                    task_list.append(json_object)
            else:
                response['success'] = False
                response['error'] = {'code':404, 'message':'creation date not in the selected academic year'} 
                return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)   

        response['success'] = True
        response['data'] = task_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)



    @http.route('/api/student/<int:student_id>/timetable', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_children(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
              
        res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
        if student_id not in res_partner.student_ids.ids:
            response['success'] = False
            response['error'] = {'code':404, 'message':'You need to be the parent of the student in order to get his info !'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        studnet_id = request.env['school.student'].search([('id','=',student_id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)            

        periode = self._default_timing_periode()
        if periode == False:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No periode defined in the system'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)         

        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        time_tables = request.env['school.time.table'].search(['&',('academic_year','=',current_academic_year.id), ('class_id','=',student_id.class_id.id)])
        if not time_tables:
            response['success'] = False
            response['error'] = {'code':404, 'message':"No timetable defined for the student's class"} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        timetable = None
        for rec in time_tables:
            if rec.timing_periode.id == periode.id:
                timetable = rec
                break
        if timetable == None:
            response['success'] = False
            response['error'] = {'code':404, 'message':"No timetable defined for the current periode"} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        session_list = []
        for session in timetable.time_table_session_ids:
            json_session = {
                        'class':session.class_id.name,
                        'subject':session.subject_id.name,
                        'teacher':session.teacher_id.name,
                        'classroom':session.classroom_id.name,
                        'start_time':session.start_time,
                        'end_time':session.end_time,
                        'day_of_week':session.day_of_week,
                    }
            session_list.append(json_session)
            
        response['success'] = True
        response['data'] = session_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)


    def _default_timing_periode(self):
        # check if there's a selected timing system
        periodes = request.env['school.timing.system.period'].search([])
        for rec in periodes:
            # reconstruct date with the current year
            start_period = str(datetime.now().year)+'-'+rec.start_date_month
            end_period = str(datetime.now().year)+'-'+rec.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                return rec
        return False

    @http.route('/api/academic_year', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_academic_year(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        academic_years = request.env['academic.year'].search([])
        academic_year=[]
        for rec in academic_years:
            json_year={
                       'id':rec.id,
                       'name':rec.name,
                       'code':rec.code,
                       'active_year':rec.active_year,
                        }
            academic_year.append(json_year)

        response['success'] = True
        response['data'] = academic_year
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

    @http.route('/api/academic_year/<int:academic_year_id>/periods', type='http', auth="user", methods=['GET'],  csrf=False)
    def _get_period_by_academic_year(self,academic_year_id,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        period=[]
        active_period= False
        academic_years = request.env['academic.year'].search([('id','=',academic_year_id)])
        periodes = request.env['school.timing.system.period'].search([])
        for rec in periodes:
            if academic_years.start_date <= rec.start_date <= academic_years.end_date:
                start_period = str(datetime.now().year)+'-'+rec.start_date_month
                end_period = str(datetime.now().year)+'-'+rec.end_date_month
                # convert date sting to date object
                start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
                end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
                # check in which period we are right now
                if start_period <= datetime.now().date() <= end_period:
                    active_period=True 
                json_period={
                           'id':rec.id,
                           'name':rec.name,
                           'active_period':active_period,
                           'start_date':rec.start_date,
                           'end_date':rec.end_date,
                }
                period.append(json_period)
                active_period= False
                
        response['success'] = True
        response['data'] = period
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
    
    @http.route('/api/class/<int:class_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_classe(self,class_id,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        classe=request.env['school.class'].search([('id','=',class_id)])
        json_class={
                    'id':classe.id,
                    'name':classe.name,
                    'code':classe.code,
                        }
       

        response['success'] = True
        response['data'] = json_class
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)


    @http.route('/api/student/attendance', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_attendance_by_student(self, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)     


        class_id = student_id.class_id
        academic_year_id = request.env['academic.year'].search([('active_year','=',True)])
        attendance_id = request.env['school.attendance'].search(['&',('class_id','=',class_id.id),('academic_year','=',academic_year_id.id)])

        att_list = []
        for att in attendance_id:
            for line in att.attendance_line_ids:
                if line.student_id.id == student_id.id and line.state != 'present':
                    att_list.append({
                        'id':att.id,
                        'teacher':{
                        'id':att.teacher_id.id,
                        'name':att.teacher_id.name,
                        'last_name':att.teacher_id.last_name,
                            },
                        'subject':{
                        'id':att.subject_id.id,
                        'name':att.subject_id.name,
                        'code':att.subject_id.code,
                            },
                        'session':{
                        'start_time':att.session_id.start_time,
                        'end_time':att.session_id.end_time,
                        'dow':att.session_id.day_of_week,
                            },
                        #'periode':{
                        #'id':att.timing_system_periode_id.id,
                        #'name':att.timing_system_periode_id.name,
                        #'start_date':att.timing_system_periode_id.start_date,
                        #'end_date':att.timing_system_periode_id.end_date,

                            #},
                        'date':att.datetime,
                        'state':line.state,
                        })
                    break
        print "#### DONE "
        response['success'] = True
        response['data'] = att_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
        
    @http.route('/api/student/tasks', type='http', auth="user", methods=['GET'],  csrf=False)
    def student_task_student(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
              
        

        student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER) 
        _logger.warning('####" STUDENT %s',student_id.name)


        tasks = request.env['school.task'].search([('state','=','approved')])
        task_list = []


        for task in tasks:
            if student_id.id in task.student_ids.ids:
                files = []
                for file in task.file_ids:
                    _logger.warning('##### MODEL NAME %s',file._name)
                    _logger.warning('##### FILE NAME %s',file.file_name)
                    _logger.warning('##### FILE ID %s',file.id)
                    domain = [
                        ('res_model', '=', file._name),
                        ('res_field', '=', 'file'),
                        ('res_id', '=', file.id),
                    ]
                    attachment = request.env['ir.attachment'].search(domain)
                    _logger.warning('##### attachment %s',attachment.id)
                    base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                    url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+request.env['ir.http'].session_info()['session_id']  

                    files.append({
                            'name':file.name,
                            'description':file.description,
                            'file_name':file.file_name,
                            'file':url,
                        })

                json_object = {
                    'id':task.id,
                    'name':task.name,
                    'creation_date':task.create_date,
                    'deadline':task.dead_line,
                    'description':task.description,
                    'all_student':task.all_student,
                    'grade':{
                        'id':task.grade_id.id,
                        'name':task.grade_id.name,
                    },
                    'class':{
                        'id':task.class_id.id,
                        'name':task.class_id.name,
                    },
                    'subject':{
                        'id':task.subject_id.id,
                        'name':task.subject_id.name,
                    },
                    'teacher':{
                        'id':task.teacher_id.id,
                        'name':task.teacher_id.name,
                    },
                    'periode':{
                        'id':task.timing_system_periode_id.id,
                        'name':task.timing_system_periode_id.name,
                    },
                    'files':files,
                }
                task_list.append(json_object)
        response['success'] = True
        response['data'] = task_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

    @http.route('/api/student/attendance/academic_year/<int:academic_year_id>/period/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_attendance_by_student_academic_year_period_student(self,academic_year_id,timing_system_periode_id, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

        student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)     


        class_id = student_id.class_id
        print "#### CLASS ID ",class_id
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        selected_academic_year = request.env['academic.year'].search([('id','=',academic_year_id)])
        attendance_id = request.env['school.attendance'].search(['&',('class_id','=',class_id.id),('academic_year','=',selected_academic_year.id)])
        print "#### ATTENDANCE ",attendance_id
        att_list = []
        for att in attendance_id:
            if timing_periode.start_date <= att.datetime <= timing_periode.end_date:
                for line in att.attendance_line_ids:
                    if line.student_id.id == student_id.id :
                        att_list.append({
                            'id':att.id,
                            'teacher':{
                                'id':att.teacher_id.id,
                                'name':att.teacher_id.name,
                                'last_name':att.teacher_id.last_name,
                            },
                            'subject':{
                                'id':att.subject_id.id,
                                'name':att.subject_id.name,
                                'code':att.subject_id.code,
                            },
                            'session':{
                                'start_time':att.session_id.start_time,
                                'end_time':att.session_id.end_time,
                                'dow':att.session_id.day_of_week,
                            },
                            'motif':line.motif,
                            'duree':line.date_time,
                            'date':att.datetime,
                            'state':line.state,
                        })
                        break
                
        print "#### DONE "
        response['success'] = True
        response['data'] = att_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

    @http.route('/api/student/tasks/academic_year/<int:academic_year_id>/period/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def student_task_year_period(self,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
              
        

        student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER) 
        _logger.warning('####" STUDENT %s',student_id.name)

        selected_academic_year= request.env['academic.year'].search([('id','=',academic_year_id)])
        tasks = request.env['school.task'].search([('state','=','approved'),('timing_system_periode_id.id','=',timing_system_periode_id)])
        task_list = []


        for task in sorted(tasks, key=lambda tasks: tasks.create_date, reverse=True):
            if student_id.id in task.student_ids.ids:
                files = []
                for file in task.file_ids:
                    _logger.warning('##### MODEL NAME %s',file._name)
                    _logger.warning('##### FILE NAME %s',file.file_name)
                    _logger.warning('##### FILE ID %s',file.id)
                    domain = [
                        ('res_model', '=', file._name),
                        ('res_field', '=', 'file'),
                        ('res_id', '=', file.id),
                    ]
                    attachment = request.env['ir.attachment'].search(domain)
                    _logger.warning('##### attachment %s',attachment.id)
                    base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                    url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+request.env['ir.http'].session_info()['session_id']  

                    files.append({
                            'name':file.name,
                            'description':file.description,
                            'file_name':file.file_name,
                            'file':url,
                        })

                json_object = {
                    'id':task.id,
                    'name':task.name,
                    'creation_date':task.create_date,
                    'deadline':task.dead_line,
                    'description':task.description,
                    'all_student':task.all_student,
                    'grade':{
                        'id':task.grade_id.id,
                        'name':task.grade_id.name,
                    },
                    'class':{
                        'id':task.class_id.id,
                        'name':task.class_id.name,
                    },
                    'subject':{
                        'id':task.subject_id.id,
                        'name':task.subject_id.name,
                    },
                    'teacher':{
                        'id':task.teacher_id.id,
                        'name':task.teacher_id.name,
                    },
                    'periode':{
                        'id':task.timing_system_periode_id.id,
                        'name':task.timing_system_periode_id.name,
                    },
                    'files':files,
                }
                task_list.append(json_object)
        response['success'] = True
        response['data'] = task_list
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

    @http.route('/api/skill_teacher',type='http', auth="user", methods=['GET'],csrf=False)
    def get_teacher_skill(self,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        skill_teacher=request.env['skill.teacher'].search([])
        skills=[]
        for skill in skill_teacher:
            skill_obj ={
            'name':skill.name,
            'id'  : skill.id,

            }

            skills.append(skill_obj)
        response['success'] = True
        response['data'] = skills
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

    @http.route('/api/region',type='http', auth="user", methods=['GET'],csrf=False)
    def get_region(self,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        region_ids=request.env['school.region'].search([])
        regions=[]
        for region in region_ids:
            region_obj ={
            'name':region.name,
            'city' : region.city,
            'id' :region.id,

            }

            regions.append(region_obj)
        response['success'] = True
        response['data'] = regions
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)

    ############# get student's class by academic year############
    @http.route('/api/student/<int:student_id>/class/academic_year/<int:academic_year>',type='http',auth="user",methods=[('GET')],csrf=False)
    def get_class_by_academic_year(self,student_id,academic_year,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        classes=[]
        student_academic_year=request.env['student.academic.year'].search([('student_id','=',student_id),('academic_year_id','=',academic_year)])
        """if not student_academic_year:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student academic year found with the given IDS'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)""" 
        for rec in student_academic_year:
            json_object={
            'class':rec.class_id.id,
            'class_code':rec.class_id.code,
            'class_decission':rec.decision,
            }
            classes.append(json_object)
        response['success'] = True
        response['data'] = classes
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)


    ############# get parent of student's classmates############""
    @http.route('/api/classmates/parent/class/<int:class_id>',type='http',auth='user',methods=['get'],csrf=False)
    def get_parent_classmates_by_class(self,class_id,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        classe =request.env['school.class'].search([('id','=',class_id)])
        if not classe:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No class found with the given IDS'} 
            return http.request.make_response(json.dumps(response),SchoolStudent.HEADER) 
        parents=[]
        for rec in classe.student_ids:
            for parent in rec.parent_ids:
                    json_object={
                        'parent_name': parent.name,
                        'parent_lastname':parent.last_name,
                        'parent_phone':parent.phone,
                    }
                    parents.append(json_object)
        response['success'] = True
        response['data'] = parents
        return http.request.make_response(json.dumps(response),SchoolStudent.HEADER)
    