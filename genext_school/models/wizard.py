# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT
import logging
import json

_logger = logging.getLogger(__name__)

class TableLineSessionWizard(models.TransientModel):

    _name = 'table.line.session.wizard'
    _description = 'Create a table line session'

    name = fields.Char('name')
    
    time_table_id   = fields.Many2one('time.table', readonly=True, ondelete="cascade")
    class_id        = fields.Many2one('school.class', readonly=True, ondelete="set null")    

    subject_id      = fields.Many2one('school.subject', required=True, ondelete="set null")
    teacher_id      = fields.Many2one('hr.employee', required=True, ondelete="set null")
    classroom_id    = fields.Many2one('school.classroom', required=True, ondelete="set null")
    start_time      = fields.Char('Start time')
    end_time        = fields.Char('End time')
    day_of_week     = fields.Integer('Day of week')




    


    @api.multi
    def save_action(self):
        # get the session of the same year
        for record in self.env['time.table.session'].search([('time_table_id.academic_year','=',self.env['academic.year'].search([('active_year','=',True)]).id)]):
            start_period = str(datetime.now().year)+'-'+record.timing_periode.start_date_month
            end_period = str(datetime.now().year)+'-'+record.timing_periode.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if self.time_table_id.timing_periode.id == record.time_table_id.timing_periode.id:
                """ convert string time to datetime object to compare them """
                current_start_time = datetime.strptime(self.start_time,'%H:%M:%S')
                current_end_time = datetime.strptime(self.end_time,'%H:%M:%S')

                start_time = datetime.strptime(record.start_time,'%H:%M:%S')
                end_time = datetime.strptime(record.end_time,'%H:%M:%S')
                """ Check if teacher is teaching simultaneously two classes """
                if (record.teacher_id.id == self.teacher_id.id) and (record.day_of_week == self.day_of_week):
                    if (start_time < current_start_time < end_time) or (start_time < current_end_time < end_time):
                        raise ValidationError(_("Teacher cannot teach two classes in the same time"))

                """ check if classroom is free """
                if (record.classroom_id == self.classroom_id) and (record.day_of_week == self.day_of_week):
                    if (start_time < current_start_time < end_time) or (start_time < current_end_time < end_time):
                        raise ValidationError(_("La salle est déja attribuée pour le meme jour et heure"))

        time_table_session = {
            'name': 'name',
            'time_table_id': self.time_table_id.id, 
            'class_id':self.class_id.id,
            'subject_id':self.subject_id.id,
            'teacher_id': self.teacher_id.id,
            'classroom_id':self.classroom_id.id,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'day_of_week': self.day_of_week
        }
        _logger.warning('###### UPDATING TIME TABLE SESSION DATA %s',self.classroom_id.name)
        _logger.warning('###### UPDATING TIME TABLE SESSION DATA %s',self.subject_id.name)
        if self._context['update'] == True:
            _logger.warning('#### UPDATING A TIME TABLE SESSION ')
            session_id = self.env['time.table.session'].browse(self._context['time_table_session_id'])
            res = session_id.write(time_table_session)
            _logger.warning('##### RESULT OF UPDATE %s',res)
            #record = self.env['time.table.session'].write(time_table_session)
        else:
            _logger.warning('#### CREATING A TIME TABLE SESSION ')
            record = self.env['time.table.session'].create(time_table_session)
        return True


    @api.multi
    def delete_action(self):
        if self._context['time_table_session_id']:
            session_id = self.env['time.table.session'].browse(self._context['time_table_session_id'])
            res = session_id.unlink()
            _logger.warning('##### RESULT OF DELETING %s',res)
        return True


    @api.onchange('class_id')
    def _onchange_day_of_week(self):
        """ check if all needed attribute are set"""
        if not self.start_time or not self.end_time or not isinstance(self.day_of_week, int):
            raise ValidationError(_('Start time, end time or day of week is not valide'))
        none_free_classroom_ids = []
        for record in self.env['time.table.session'].search([]):
            start_period = str(datetime.now().year)+'-'+record.timing_periode.start_date_month
            end_period = str(datetime.now().year)+'-'+record.timing_periode.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if self.time_table_id.timing_periode.id == record.time_table_id.timing_periode.id:
                current_start_time = datetime.strptime(self.start_time,'%H:%M:%S')
                current_end_time = datetime.strptime(self.end_time,'%H:%M:%S')

                start_time = datetime.strptime(record.start_time,'%H:%M:%S')
                end_time = datetime.strptime(record.end_time,'%H:%M:%S')
                """ ids of classrooms used in the same day """
                if record.day_of_week == self.day_of_week:
                    """ ids of classrooms used in the same time range """
                    if (start_time < current_start_time < end_time) or (start_time < current_end_time < end_time):
                        none_free_classroom_ids.append(record.classroom_id.id)
        return {'domain':{'classroom_id':[('id','not in',none_free_classroom_ids)]}}


    @api.onchange('class_id')
    def _onchange_class_id(self):
        grade_id = self.env['school.grade'].browse(self.class_id.grade_id.id)
        return {'domain':{'subject_id':[('id','in',grade_id.subject_ids.ids)]}}



    @api.onchange('subject_id')
    def _onchange_subject_id(self):
        ''' ADD ACADEMIC YEAR AND TIMING PERIODE IN SEARCH <-------------------------------------------------------------------------'''
        subject = self.env['school.subject'].browse(self.subject_id.id)
        print "#### teachers ",subject.teacher_ids.ids

        none_free_teacher_ids = []
        for record in self.env['time.table.session'].search([('time_table_id.academic_year','=',self.env['academic.year'].search([('active_year','=',True)]).id)]):
            start_period = str(datetime.now().year)+'-'+record.timing_periode.start_date_month
            end_period = str(datetime.now().year)+'-'+record.timing_periode.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if self.time_table_id.timing_periode.id == record.time_table_id.timing_periode.id:
                current_start_time = datetime.strptime(self.start_time,'%H:%M:%S')
                current_end_time = datetime.strptime(self.end_time,'%H:%M:%S')

                start_time = datetime.strptime(record.start_time,'%H:%M:%S')
                end_time = datetime.strptime(record.end_time,'%H:%M:%S')
                """ ids of sessions in the same day """
                print "RECORD DAY OF WEEK ",record.day_of_week
                print "SELF DAY OF WEEK ",self.day_of_week
                if record.day_of_week == self.day_of_week:
                    print "### same week"
                    """ ids of classrooms used in the same time range """
                    if (start_time < current_start_time < end_time) or (start_time < current_end_time < end_time):
                        print "### same time"
                        """ ids of teacher used in the same time range with the same subject"""
                        if record.subject_id == self.subject_id:
                            print "### same sabject"
                            print "### NONE FREE TEACHER ",record.teacher_id.name
                            none_free_teacher_ids.append(record.teacher_id.id)

        return {'domain':{'teacher_id':['&',('id','not in',none_free_teacher_ids),'&',('is_school_teacher','=',True),('id','in',subject.teacher_ids.ids)]}}



    @api.model
    def default_get(self, fields):
        res = super(TableLineSessionWizard, self).default_get(fields)
        """ create a new time.table.session record and link it to the current time.table record"""
        _logger.warning('######## DEFAULT GET CONTEXT %s',self._context)
        if not self._context.get('update', False):
            _logger.warning('####### CREATING NEW SESSSION %s',self._context)
            if not self._context.get('time_table_id',False):
                raise ValidationError(_('time.table id is not valide [table.line.session.wizard] in context[]'))
            else:
                record = self.env['time.table'].browse(self._context['time_table_id'])
                if len(record):
                    start_date_object = datetime.strptime(self._context['start'], '%Y-%m-%d %H:%M:%S')
                    end_date_object = datetime.strptime(self._context['end'], '%Y-%m-%d %H:%M:%S')
                    print "START TIME FROM JS ",start_date_object
                    print "END TIME FROM JS ",end_date_object
                    
                    res['time_table_id'] = record.id
                    res['class_id'] = record.class_id.id 
                    res['subject_id'] = False
                    res['teacher_id'] = False
                    res['classroom_id'] = False
                    res['start_time'] = str(start_date_object.time())
                    res['end_time'] = str(end_date_object.time())
                    res['day_of_week'] = int(start_date_object.weekday())
                else:
                    raise ValidationError(_('time.table id is not valide [table.line.session.wizard]'))
        else:
            _logger.warning('###### UPDATING TIMETABLE SESSION %s',self._context)
            """ updating a time.table.session """
            if not self._context.get('time_table_id',False):
                raise ValidationError(_('time.table id is not valide [table.line.session.wizard] in context[]'))
            else:
                record = self.env['time.table'].browse(self._context['time_table_id'])
                session_id = self.env['time.table.session'].browse(self._context['time_table_session_id'])
                _logger.warning(' ###### FOUND SESSION ID %s',session_id.subject_id.name)
                if len(record) and len(session_id):
                    start_date_object = datetime.strptime(self._context['start'], '%Y-%m-%d %H:%M:%S')
                    end_date_object = datetime.strptime(self._context['end'], '%Y-%m-%d %H:%M:%S')
                    print "START TIME FROM JS ",start_date_object
                    print "END TIME FROM JS ",end_date_object
                    
                    res['time_table_id'] = record.id
                    res['class_id'] = session_id.class_id.id 
                    res['subject_id'] = session_id.subject_id.id 
                    res['teacher_id'] = session_id.teacher_id.id
                    res['classroom_id'] = session_id.classroom_id.id
                    res['start_time'] = str(start_date_object.time())
                    res['end_time'] = str(end_date_object.time())
                    res['day_of_week'] = int(start_date_object.weekday())
                else:
                    raise ValidationError(_('time.table id is not valide [table.line.session.wizard]'))
        return res




class StudentRegistrationWizard(models.TransientModel):
    _name = 'student.registration'
    _description='Student Registration Wizard'


    student_id              = fields.Many2one('school.student')

    educational_stage_id    = fields.Many2one('educational.stage',required=True, string="Educational Stage", ondelete='cascade')
    academic_year           = fields.Many2one('academic.year',required=True, default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), ondelete="set null")
    educational_stage_id    = fields.Many2one('educational.stage', required=True, ondelete="set null")
    orientation_system      = fields.Selection(related='educational_stage_id.orientation_system', ondelete="set null")
    field_id                = fields.Many2one('school.field', domain="[('educational_stage_id','=',educational_stage_id)]", ondelete="set null")
    grade_id                = fields.Many2one('school.grade', required=True,
        domain="['|','&',('field_id','=',field_id),('field_id','!=',False),'&',('educational_stage_id','=',educational_stage_id),('field_id','=',False)]", ondelete="set null")
    class_id                = fields.Many2one('school.class', domain="[('grade_id', '=', grade_id)]", required=True,ondelete="set null")
    
    
    @api.model
    def view_init(self, fields_list):
        """ Override this method to do specific things when a form view is
        opened. This method is invoked by :meth:`~default_get`.
        """
        print "CALL TO VIEW INIT"
        pass

    @api.model
    def default_get(self, fields):
        res = super(StudentRegistrationWizard, self).default_get(fields)
        if self._context.get('update', False):
            if not self._context.get('student_academic_year_id',False):
                raise ValidationError(_('Error student_academic_year_id id is not valid [UPDATING STUDENT ACADEMIC YEAR]'))
            else:
                record = self.env['student.academic.year'].search([('id','=',self._context['student_academic_year_id'])])
                res['student_id'] = record.student_id.id
                res['academic_year'] = record.academic_year_id.id
                res['educational_stage_id'] = record.educational_stage_id.id
                res['field_id'] = record.field_id.id
                res['grade_id'] = record.grade_id.id
                res['class_id'] = record.class_id.id
        # print "###################### STUDENT ID ",res
        return res

    # @api.multi
    # def register_action(self):
    #     print "################ REGISTER ACTION : "
    #     # self._context['student_id'] is from the student action
    #     if self._context.get('update',False) and self._context.get('student_academic_year_id',False):
    #         # do not update the student id
    #         print "CONTENT OF FIELD ID ",self.field_id
    #         print "CONTENT OF FIELD ID ",self.orientation_system
    #         # check if field id is ignored, it may still hold old value
    #         field_id = False
    #         if self.orientation_system == 'no-field':
    #             field_id= False
    #         else:
    #             field_id = self.field_id.id
    #         student_academic_year = {
    #                                 'academic_year_id':self.academic_year.id,
    #                                 'educational_stage_id':self.educational_stage_id.id,
    #                                 'field_id': field_id,
    #                                 'grade_id': self.grade_id.id, 
    #                                 'class_id':self.class_id.id
    #                                 }
    #         record = self.env['student.academic.year'].search([('id','=',self._context['student_academic_year_id'])])
    #         record.write(student_academic_year)
    #         return {'type': 'ir.actions.client','tag': 'reload'}
    #     else:
    #         print "###################### REGISTER ACTION ELSE"
    #         print "######################### STUDENT",self._context.get('update')
    #         if not self._context.get('student_id',False):
    #             # print "###################### STUDENT ID",self._context['student_id']
    #             raise ValidationError(_('Error student id is not valid [Student registration]]'))
    #         student = self.env['school.student'].search([('id','=',self._context['student_id'])])

    #         for record in student.student_academic_year:
    #             if record.decision == 'in-progress':
    #                 raise ValidationError(_('Action cannot be performed. Students current year still in progress!'))
    #         student_academic_year = {
    #                                 'student_id': student.id, 
    #                                 'academic_year_id':self.academic_year.id,
    #                                 'educational_stage_id':self.educational_stage_id.id,
    #                                 'field_id': self.field_id.id,
    #                                 'grade_id': self.grade_id.id, 
    #                                 'class_id':self.class_id.id
    #                                 }
    #         student_academic_year_object = self.env['student.academic.year'].create(student_academic_year)
    #         student.write({'student_academic_year': student_academic_year_object,
    #                        'current_student_academic_year':student_academic_year_object.id, 'state':'pre-registred'})
    #         return {'type': 'ir.actions.client','tag': 'reload'}

    @api.multi
    def register_action(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['school.student'].browse(active_ids):
            for year in record.student_academic_year:
                if year.decision == 'in-progress':
                    raise ValidationError(_('Action cannot be performed. Students current year still in progress!'))
            student_academic_year = {
                                    'student_id': record.id, 
                                    'academic_year_id':self.academic_year.id,
                                    'educational_stage_id':self.educational_stage_id.id,
                                    'field_id': self.field_id.id,
                                    'grade_id': self.grade_id.id, 
                                    'class_id':self.class_id.id
                                    }
            print "################", student_academic_year
            student_academic_year_object = self.env['student.academic.year'].create(student_academic_year)
            record.write({'student_academic_year': student_academic_year_object,
                           'current_student_academic_year':student_academic_year_object.id, 'state':'pre-registred'})
        return {'type': 'ir.actions.client','tag': 'reload'}



   
