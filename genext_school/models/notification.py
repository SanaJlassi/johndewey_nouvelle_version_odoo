# -*- coding: utf-8 -*-
import cookielib
import urllib2
import json
import base64


class Client:

    LOGIN_URL       = 'api/signin'
    SEND_SMS        = 'api/send-sms'
    COUNT_SMS       = 'api/counts-sms'
    UPDATE_DETAILS  = 'api/edit-user'
    GET_HEADER      = 'api/contact-headers'
    EDIT_HEADER     = 'api/edit-header'
    SENT_SMS        = 'api/list-sms'
    NOT_SENT_SMS    = 'api/drafts'
    

    def __init__(self, url='https://onesignal.com/api/v1/notifications',debug=0):
        self.cookies = cookielib.CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPHandler(),urllib2.HTTPCookieProcessor(self.cookies))
        self.headers = {'Content-Type':'application/json', 'Accept':'application/json'}
        self.url = url
        self.app_id = None
        self.debug = debug


    def sendPushNotification(self, data=None ,content=None, tokens=[]):
        if content == None:
            content = {'en':'John Dewey school'}

        data_to_send = {"app_id" : "3e020c69-ee85-4e61-8d0e-3c8ee7180130","contents":content,"include_player_ids":tokens, "data":data}
        data_to_send = json.dumps(data_to_send)
        request =  NotificationRequest(self.url, data=data_to_send, headers=self.headers, method='POST')
        serverResponse = None
        response = {'success':False,'error':[]}
        try:
            if self.debug == 1:
                print 'Sending Notification'
            serverResponse = self.opener.open(request)
            if self.debug == 1:
                print 'Notification sent with success'
        except urllib2.HTTPError ,e:
            response['error'].append('Error while calling server')
        except urllib2.URLError, e:
            print 'Unable to connect'
            response['error'].append('Unable to open url, Connection refused')

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            
            return data
        else:
            response['success'] = False
            return response



class NotificationRequest(urllib2.Request):
    """
    Extending the urllib2.Request class in order to add additional request method
    to GET and POST like PUT and DELETE 
    """
    def __init__(self, url, data=None, headers={}, origin_req_host=None, unverifiable=False, method=None):
        urllib2.Request.__init__(self, url, data, headers, origin_req_host, unverifiable)
        self.httpMethod = method

    def get_method(self):
        if self.httpMethod is not None:
            return self.httpMethod
        else:
            return urllib2.Request.get_method(self)

    def set_method(self, method):
        self.httpMethod = method



def printException(exception):
    print '---- > Unkown Error occured : \n\tURL : %s \n\tCODE : %s \n\tMSG : %s' % (exception.url, exception.code, exception.msg)
    jsonData = json.loads(exception.fp.read())
    if jsonData is not None:
        print json.dumps(jsonData, indent=4, sort_keys=True)    
    else:
        print 'Response is not of type JSON\n'


if __name__ == '__main__':
    client = Client(url='http://192.168.1.10:8080',debug=1)
    response = client.loginUser('admin','Nevermind')
    user = {
      "id":"amir",
      "firstName": "amir",
      "lastName": "ben ahmed",
      "email": "abdel@example.com",
      "password": "Nevermind",
    }
    print client.createUser(user)