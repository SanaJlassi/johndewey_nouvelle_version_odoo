# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError




class SchoolConfigSettings(models.TransientModel):

    _name = 'school.config.settings'
    _inherit = 'res.config.settings'

    field_naming_pattern        = fields.Selection(
                                        [
                                            ('code-name','Code/Name'),
                                            ('name-code','Name/Code'),
                                            ('name','Name'), 
                                            ('code','Code')
                                        ], default='code-name')
    institution_type            = fields.Selection(
                                        [
                                            ('elementary-school','Elementary School'),
                                            ('middle-school','Middle School'),
                                            ('high-school','High School'),
                                            ('university','University'),
                                        ], default="elementary-school")


    min_student_age                          = fields.Integer("Minimum student age", default=5)
    class_name_prefix                        = fields.Char("Class name prefix", default='Group')
    default_allow_max_student_number         = fields.Boolean("Allow max student number", default=False, default_model='school.class')
    default_max_student_number               = fields.Integer("Max Student number per class", default=30, default_model='school.class')


    
    

    @api.multi
    def set_class_name_prefix_defaults(self):
        # make sure the default value is set if none of other choices is selected
        if not self.class_name_prefix:
            raise UserError("Class name prefix must contain at least one character")
        return self.env['ir.values'].sudo().set_default('school.config.settings', 'class_name_prefix', self.class_name_prefix)


    @api.multi
    def set_field_naming_pattern_defaults(self):
        # make sure the default value is set if none of other choices is selected
        if not self.field_naming_pattern:
            self.field_naming_pattern = 'code-name'
        return self.env['ir.values'].sudo().set_default('school.config.settings', 'field_naming_pattern', self.field_naming_pattern)

    @api.multi
    def set_institution_type_defaults(self):
        # make sure the default value is set if none of other choices is selected
        if not self.institution_type:
            self.institution_type = 'elementary-school'
        return self.env['ir.values'].sudo().set_default('school.config.settings', 'institution_type', self.institution_type)

    @api.multi
    def set_min_student_age_defaults(self):
        # make sure the minimum age is greater than 0
        if self.min_student_age == 0:
            raise UserError("Minimum age cannot be set to 0 !")
        return self.env['ir.values'].sudo().set_default('school.config.settings', 'min_student_age', self.min_student_age)

    