# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)


class Teacher(models.Model):
    
    #_name = 'hr.employee'
    _inherit = 'hr.employee'
    _order = "pid asc"
    _description="School Teacher|Employee"


    @api.depends('user_id') 
    @api.multi
    def _compute_res_partner(self):
        for rec in self:
            _logger.warning("### setting partner")
            if rec.user_id and rec.user_id.partner_id:
                _logger.warning("### user partner %s",rec.user_id.partner_id.name)
                rec.partner_id = rec.user_id.partner_id
                rec.partner_id.write({'is_school_teacher':True, 'name':rec.name,'last_name':rec.last_name,'pid':rec.pid})
                _logger.warning("#### Teacher PARTNER %s",rec.partner_id.name)
                _logger.warning("#### Teacher last_name %s",rec.partner_id.last_name)
                _logger.warning("#### Teacher is_school_student %s",rec.partner_id.is_school_teacher)
                _logger.warning("#### Teacher is_school_student %s",rec.partner_id.pid)


    partner_id              = fields.Many2one('res.partner', ondelete='restrict', string='Related System partner',store=True, compute="_compute_res_partner")
    last_name               = fields.Char('Nom')
    pid                     = fields.Char('Identifiant', help="Numéro d'identification personnelle")
    user_type               = fields.Selection(related='user_id.user_type')
    is_school_teacher       = fields.Boolean('School Teacher')
    subject_ids             = fields.Many2many('school.subject', compute='_compute_subject', string="Matières", store=True)
    cin                     = fields.Char(string="CIN")
    contact_ids             = fields.Many2many('res.partner', compute='_compute_contact_ids', string="Contacts")
    class_ids               = fields.Many2many('school.class', compute='_compute_contact_ids', string="Classes")
    is_primary              = fields.Boolean(string="Enseignant principal", default=False)
    password                = fields.Char(related='user_id.partner_id.password')
    birthday_city           = fields.Char('Lieu de naissance') #Many2one("res.country.state", string='Lieu de naissance')
    spouse                  = fields.Many2one('res.partner', ondelete='restrict', string='Conjoint' )
    employee_child_ids      = fields.Many2many('res.partner', string='Enfants')
    street                  = fields.Char(related='address_home_id.street', string="Adresse")
    city                    = fields.Char(related='address_home_id.city', string="Ville")
    zip_code                = fields.Char(related='address_home_id.zip', string="Code postal")
    education_ids           = fields.One2many('hr.education', 'employee_id', string='Études et diplômes')
    experience_ids          = fields.One2many('hr.experience', 'employee_id', string='Expériences professionnelles')
    job_id                  = fields.Many2one('hr.job', string='Nature du poste')
    job_line_id             = fields.One2many('hr.job.line', 'employee_id', string="Poste")
    skill_ids               = fields.Many2many('school.skill',compute='_compute_skill',string="Compétence", store=True)
    
    school_id     = fields.Many2many('school.etablissement',compute='_get_teacher_school')

    allow_platform_login    = fields.Boolean(string="Autoriser la connexion à la plateforme",default=False,required=True)
    random_key = fields.Char(related='user_id.random_key',string='Code de parrainage')
    region_ids             = fields.Many2many('school.region', compute='_compute_region', string="Matières", store=True)
    skill_teachers_ids     = fields.Many2many('skill.teacher.option',compute='_compute_skill_teacher',string="Compétences Enseignant")
    def _get_teacher_school(self):
        etablissement_obj= self.env['school.etablissement'].search([])
        for etablissement in etablissement_obj:
            if etablissement.school_active == True:
                self.school_id = etablissement

    @api.multi
    def _compute_contact_ids(self):
        for record in self:
            classes, parents = self._get_class_and_parents(record.id)
            # print "### parent ids ",parents
            # print "### classe ids ",classes
            record.contact_ids = self.env['res.partner'].browse(parents)
            record.class_ids = self.env['school.class'].browse(classes)

    def _get_class_and_parents(self, teacher_id):
        ''' 
            return the ids of teachers who's teaching the given class in current timing periode 
        '''
        _logger.warning("### TEACHER ID %s",teacher_id)
        classes = []
        active_academic_year = self.env['academic.year'].search([('active_year','=',True)])
        recs = self.env['time.table'].search([('academic_year','=',active_academic_year.id)])
        for rec in recs:
            # print "## name of time table ",rec.name
            # print "## name timing_system ",rec.timing_system.name
            # print "## name timing_periode ",rec.timing_periode.name
            timing_period_id = rec.timing_periode

            start_period = str(datetime.now().year)+'-'+timing_period_id.start_date_month
            end_period = str(datetime.now().year)+'-'+timing_period_id.end_date_month
            # convert date string to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                _logger.warning(" FOUND CURRENT PERIODE %s",timing_period_id.name)
                for session in rec.time_table_session_ids:
                    _logger.warning("#### SESSION teacher %s",session.teacher_id.name)
                    if session.teacher_id.id == teacher_id:
                        _logger.warning("TEACHER NAME %s",session.teacher_id.name)
                        classes.append(rec.class_id.id)
                        break
        classes = list(set(classes))
        parents = []
        for classe in self.env['school.class'].browse(classes):
            students = self.env['school.student'].search([('class_id','=',classe.id)])
            for student in students:
                for parent in student.parent_ids:
                    parents.append(parent.id)

        return classes, parents


    # @api.onchange('country_id')
    # def _onchange_country_id(self):
    #     if self.country_id:
    #         return {'domain': {'birthday_city': [('country_id', '=', self.country_id.id)]}}
    #     else:
    #         return {'domain': {'birthday_city': []}}

    @api.multi
    def name_get(self):
        return [(record.id, record.name+' '+record.last_name if record.last_name else record.name)for record in self]
        

    @api.multi
    def _compute_subject(self):
        ''' This function will automatically computes the subjects related to particular teacher.'''
        subject_obj = self.env['school.subject']
        for rec in self:
            # Search the subject assign to teacher
            subject_ids = subject_obj.search([('teacher_ids', '=', rec.id)])
            # append the subjects
            rec.subject_ids = False
            if subject_ids:
                # check if the teacher is teaching any primary subject
                is_primary = False
                for subject in subject_ids:
                    if subject.is_primary:
                        is_primary = True
                # make the teacher primary if he's teaching any primary subject otherwise is_primary = False
                if is_primary:
                    rec.is_primary = True
                else:
                    rec.is_primary = False
                rec.subject_ids = [subject.id for subject in subject_ids]


    @api.multi
    def _compute_skill_teacher(self):
        skill_teacher_obj =self.env['skill.teacher.option']
        for rec in self:
            skill_teacher = skill_teacher_obj.search([('teacher_id','=',rec.id)])
        rec.skill_teachers_ids = skill_teacher
    @api.multi
    def _compute_skill(self):
        skill_obj = self.env['school.skill']
        for rec in self:
            skill_ids = skill_obj.search([('teacher_ids', '=', rec.id)])
        rec.skill_ids = skill_ids

    @api.model
    def create(self, vals):
        '''This method creates teacher user and assign group teacher'''
        vals['pid'] = self.env['ir.sequence'].next_by_code('hr.employee')
        res = super(Teacher, self).create(vals)
        if res.is_school_teacher:
            user_vals = {'name': vals.get('name'),
                         'login': vals.get('pid', False),
                         'password': vals.get('pid', False),
                         'user_type': 'teacher',
                         'partner_id': self.id,'allow_platform_login':vals.get('allow_platform_login', False)}
            # Create user
            user = self.env['res.users'].create(user_vals)
            if user and user.partner_id:
                user.partner_id.write({'email': vals.get('work_email', False),'phone':vals.get('mobile_phone', False)})
            # Assign group of teacher to user created
            if res and user:
                res.write({'address_home_id': user.partner_id.id,
                           'user_id': user and user.id or False})
                teacher_group = self.env.ref('genext_school.group_school_teacher')
                emp_group = self.env.ref('base.group_user')
                user.write({'groups_id': [(6, 0, [emp_group.id, teacher_group.id])]})
        return res
    @api.multi
    def _compute_region(self):
        ''' This function will automatically computes the regions related to particular teacher.'''
        region_obj = self.env['school.region']
        for rec in self:
            # Search the region assign to teacher
            regions_ids = region_obj.search([('teacher_ids', '=', rec.id)])
            
            rec.region_ids = False
            if regions_ids:
           
                rec.region_ids = [region.id for region in regions_ids]

    @api.multi
    def write(self, vals):
        '''Write method of hr employee'''
        res = super(Teacher, self).write(vals)
        # creating user
        for rec in self:
            # if is_school_teacher is checked -- > create a user
            if rec.is_school_teacher and not rec.user_id:
                user_vals = {'name': rec.name,
                             'login': rec.pid,
                             'password': rec.pid,
                             'user_type': 'teacher',
                             'partner_id': rec.id}
                # Create user
                user = self.env['res.users'].create(user_vals)
                if user and user.partner_id:
                    user.partner_id.write({'email': rec.work_email, 'phone':rec.mobile_phone if rec.mobile_phone else rec.work_phone})
                # Assign group of teacher to user created
                if res and user:
                    rec.write({'address_home_id': user.partner_id.id,'user_id': user and user.id or False})
                    teacher_group = self.env.ref('genext_school.group_school_teacher')
                    emp_group = self.env.ref('base.group_user')
                    user.write({'groups_id': [(6, 0, [emp_group.id, teacher_group.id])]})

        for rec in self:
            # Assign email
            if rec.is_school_teacher and rec.user_id:
                if vals.get('login'):
                    rec.user_id.write({'login': vals.get('login')})
                # Assign name
                if vals.get('name',False):
                    rec.user_id.write({'name': vals.get('name')})
                    if rec.user_id.partner_id:
                        rec.user_id.partner_id.write({'name': vals.get('name')})
                if vals.get('last_name',False):
                    if rec.user_id.partner_id:
                        rec.user_id.partner_id.write({'last_name': vals.get('last_name')})
                if vals.get('mobile_phone',False):
                    if rec.user_id.partner_id:
                        rec.user_id.partner_id.write({'phone': vals.get('mobile_phone')})
                if vals.get('allow_platform_login', False):
                    rec.user_id.write({'allow_platform_login': vals.get('allow_platform_login')})
        return res

    @api.multi
    def unlink(self):
        recordset = self.env['res.users']
        ''' deleting the user related to  hr.employee which are school teachers'''
        for rec in self:
            if rec.is_school_teacher:
                user = rec.mapped('resource_id.user_id')
                recordset += user
        super(Teacher, self).unlink()
        return recordset.unlink()


class HrEducation(models.Model):
    _name = 'hr.education'
    _description = 'Employee Education'

    employee_id                 = fields.Many2one('hr.employee', ondelete='cascade')
    name                        = fields.Char('Name')
    field                       = fields.Char('Spécialité')
    graduation_year             = fields.Date("Année d'obtention")
    institution                 = fields.Char('Établissement')
    diploma_prototype           = fields.Binary("Joindre un prototype")
    description                 = fields.Text("Description of diploma")

class HrExperience(models.Model):
    _name = 'hr.experience'
    _description = 'Employee Experience'

    employee_id                 = fields.Many2one('hr.employee', ondelete='cascade')
    name                        = fields.Char('Intitulé du diplôme')
    employer                    = fields.Char('Employeur')
    start_date                  = fields.Date("Date de début")
    end_date                    = fields.Date('Date de fin')
    file                        = fields.Binary("Joindre un fichier")
    description                 = fields.Text("Description of Experience")

class HrJob(models.Model):
    _name = 'hr.job.line'
    _description = 'Employee Job'

    name                        = fields.Char('Titre du poste')
    employee_id                 = fields.Many2one('hr.employee', ondelete='cascade')
    job_position_id             = fields.Many2one(related='employee_id.job_id', string="Nature du poste")
    department_id               = fields.Many2one(related='employee_id.department_id', string="Département")
    work_location               = fields.Char(related='employee_id.work_location', string="Lieu de travail")
    address_id                  = fields.Many2one(related='employee_id.address_id', string="Adresse professionnelle")
    start_date                  = fields.Date("Date d'embauche")
    end_date                    = fields.Date('Date de sortie')    
    exit_reason                 = fields.Text("Motif de sortie")
    grade                       = fields.Char("Grade")
    echelon                     = fields.Char("Échelon")
    status                      = fields.Selection([('temporary','Titulaire'),('contractual','Contractuel'),('permanent','Permanent'),('intern','Stagiaire')], string="Statut")
    bank_account_id             = fields.Many2one(related='employee_id.bank_account_id', string="Coordonnées bancaires")
    working_hours               = fields.Float("Nombre d'heures")
    class_ids                   = fields.Many2many(related='employee_id.class_ids', string="Niveaux affectés")
    subject_ids                 = fields.Many2many(related='employee_id.subject_ids', string="Discipline(s) enseignée(s)")
    calendar_id                 = fields.Many2one(related='employee_id.calendar_id', string="Temps de travail")



class HrPersonalInfoConfig(models.Model):
    _name = 'hr.civil.status.config'
    _description = 'Employee Civil Status config'

    name                        = fields.Char(string='Nom')
    code                        = fields.Selection([
                                                        ('name','Nom'), 
                                                        ('last_name','Prénom'), 
                                                        ('gender','Sexe'), 
                                                        ('marital','Statut'),
                                                        ('birthday','Date de naissance'),
                                                        ('birthday_city','Lieu de naissance'),
                                                        ('country_id','Nationalité'),
                                                        ('street','Adresse'),
                                                        ('city','Ville'),
                                                        ('zip','Code postal'),
                                                        ('cin','N° CIN'),
                                                        ('identification_id','N° Titre de sejour'),
                                                        ('ssnid','N° CNSS'),
                                                        ('mobile_phone','Téléphone'),
                                                        ('work_email','Email'),
                                                        ('spouse','Conjoint'),
                                                        ('employee_child_ids','Enfants'),
                                                        ('bank_account_id','Coordonnées bancaires'),
                                                        ], string='Code')
    @api.multi
    def name_get(self):
        return [(record.id,record.code)for record in self]

class HrEducationConfig(models.Model):
    _name = 'hr.education.config'
    _description = 'Employee education config'

    name                        = fields.Char(string='Nom')
    code                        = fields.Selection([
                                                        ('name','Intitulé du diplôme'), 
                                                        ('field','Spécialité'), 
                                                        ('graduation_year',"Année d'obtention"), 
                                                        ('institution','Établissement'),
                                                        ], string='Code')
    @api.multi
    def name_get(self):
        return [(record.id,record.code)for record in self]

class HrExperienceConfig(models.Model):
    _name = 'hr.experience.config'
    _description = 'Employee experience config'

    name                        = fields.Char(string='Nom')
    code                        = fields.Selection([
                                                        ('name','Poste'), 
                                                        ('employer','Employeur'), 
                                                        ('start_date','Date de début'), 
                                                        ('end_date','Date de fin'),
                                                        ], string='Code')
    @api.multi
    def name_get(self):
        return [(record.id,record.code)for record in self]

class HrJobConfig(models.Model):
    _name = 'hr.job.config'
    _description = 'Employee job config'

    name                        = fields.Char(string='Nom')
    code                        = fields.Selection([
                                                        ('name','Titre du poste'),
                                                        ('department_id','Département'),
                                                        ('job_position_id','Nature du poste'),
                                                        ("start_date","Date d'embauche"), 
                                                        ('end_date','Date de sortie'), 
                                                        ('exit_reason',"Motif de sortie"), 
                                                        ('grade','Grade'),
                                                        ('echelon','Échelon'),
                                                        ('status','Statut'),
                                                        ('working_hours',"Nombre d'heures"),
                                                        ('calendar_id','Temps de travail'),
                                                        ('class_ids','Niveaux affectés'),
                                                        ('subject_ids','Discipline(s) enseignée(s)'),
                                                        ], string='Code')
    @api.multi
    def name_get(self):
        return [(record.id,record.code)for record in self]


class IrModelFields(models.Model):
    _inherit = 'ir.model.fields'

    model_name          = fields.Char(related='model_id.model',help="The model this field belongs to")
