{
    'name': 'Hide menu',
    'version': '1.0',
    'category': 'School Management',
    'author': 'Genext Team',
    'website': 'https://www.genext-it.com',
    'depends': ['base'],
    'data': [
        'views/hide_menu.xml',
    ],
    'qweb': ['static/src/xml/*.xml'],
    'images':['static/images/genext.png',],
    'installable': True,
    'auto_install': False,
    'application': True,
}