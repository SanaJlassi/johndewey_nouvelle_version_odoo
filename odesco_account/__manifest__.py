{
    'name': 'Odesco Account Module',
    'version': '1.0',
    'category': 'School Management',
    'author': 'Genext Team',
    'website': 'https://www.genext-it.com',
    'depends': ['base'],
    'data': [
        'views/fees.xml',
        'menu/menuitem.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}