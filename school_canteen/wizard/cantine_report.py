# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes




class CantineReport(models.TransientModel):
    _name = 'school.cantine.report'

    def _default_today_reservation(self):
        self.menu_id = self.env['school.menu'].search([('from_date','=',fields.Date.today())],limit=1)
        self.reservation_ids = self.env['school.reservation'].search(['&',('menu_id','in',self.menu_id.ids),('state','=','accepted')])

    @api.model
    def default_get(self, fields):
        res = super(CantineReport, self).default_get(fields)
        menu_id = self.env['school.menu']
        menu_id = self.env['school.menu'].search([('from_date','=',attributes.Date.today())],limit=1)
        reservation_ids = self.env['school.reservation']
        if menu_id:
        	reservation_ids = self.env['school.reservation'].search(['&',('menu_id','in',menu_id.ids),('state','=','accepted')])

        print "##### menu_id ",menu_id
        print "##### reservation_ids ",reservation_ids
                    
        res['menu_id'] = menu_id.id if menu_id else False
        res['reservation_ids'] = reservation_ids.ids if reservation_ids else False
        return res


    
    reservation_ids  = fields.Many2many('school.reservation',string="Réservation")
    menu_id          = fields.Many2one('school.menu',string='Menu')



    @api.multi
    def generate_report(self):
        reservations = []
        for res in self.reservation_ids:
            rec = {'name':res.name, 'student_name':res.student_id.name, 'student_last_name':res.student_id.last_name, 'state':res.state}
            reservations.append(rec)


        lines = []
        for line in self.menu_id.menu_line_ids:
            lines.append({'name':line.name, 'type':line.meal_id.meal_type_id.name, 'meal':line.meal_id.name})

        menu = {'name':self.menu_id.name, 'date':self.menu_id.from_date, 'meals':lines}

        data = {'reservations': reservations, 'menu_id': menu}
        return self.env['report'].get_action(self, 'school_canteen.cantine_report', data=data) 


    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['reservation_ids', 'menu_id'])[0]
        print "######### DATA ",data
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['reservation_ids', 'menu_id'])[0])
        print "####### UPDATED DATA ",data
        return self.env['report'].get_action(self, 'school_canteen.cantine_report', data=data)

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        sales_records = []
        orders = self.env['school.reservation'].search([])
        # if docs.date_from and docs.date_to:
        #    for order in orders:
        #      if parse(docs.date_from) <= parse(order.date_order) and parse(docs.date_to) >= parse(order.date_order):
        #          sales_records.append(order);
        #      else:
        #          raise UserError("Please enter duration")

        docargs = {
           'doc_ids': self.ids,
           'doc_model': self.model,
           'docs': docs,
           'data':data['form'],
           'time': time,
           'orders': orders
        }
        print "######## BEFORE PRiNTiNG REPORT ",self
        return self.env['report'].render('school_canteen.cantine_report', docargs)