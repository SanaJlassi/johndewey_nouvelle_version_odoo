# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json


class Cashier(models.Model):
    _inherit = 'hr.employee'
    _order = "pid asc"
    _description = "School Cashier"

    is_school_cashier = fields.Boolean('School Cashier')

    @api.model
    def create(self, vals):
        '''This method creates teacher user and assign group teacher'''
        vals['pid'] = self.env['ir.sequence'].next_by_code('hr.employee')
        res = super(Cashier, self).create(vals)
        if res.is_school_cashier:
            user_vals = {'name': vals.get('name'),
                         'login': vals.get('pid', False),
                         'password': vals.get('pid', False),
                         'user_type': 'teacher',
                         'partner_id': self.id}
            # Create user
            user = self.env['res.users'].create(user_vals)
            if user and user.partner_id:
                user.partner_id.write(
                    {'email': vals.get('work_email', False), 'phone': vals.get('mobile_phone', False)})
            # Assign group of teacher to user created
            if res and user:
                res.write({'address_home_id': user.partner_id.id,
                           'user_id': user and user.id or False})
                cashier_group = self.env.ref('odesco_access_right.group_school_cashier')
                emp_group = self.env.ref('base.group_user')
                user.write({'groups_id': [(6, 0, [emp_group.id, cashier_group.id])]})
        return res

    @api.multi
    def write(self, vals):
        '''Write method of hr employee'''
        res = super(Cashier, self).write(vals)
        # creating user
        for rec in self:
            # if is_school_teacher is checked -- > create a user
            if rec.is_school_cashier and not rec.user_id:
                user_vals = {'name': rec.name,
                             'login': rec.pid,
                             'password': rec.pid,
                             'user_type': 'teacher',
                             'partner_id': rec.id}
                # Create user
                user = self.env['res.users'].create(user_vals)
                if user and user.partner_id:
                    user.partner_id.write(
                        {'email': rec.work_email, 'phone': rec.mobile_phone if rec.mobile_phone else rec.work_phone})
                # Assign group of teacher to user created
                if res and user:
                    rec.write({'address_home_id': user.partner_id.id, 'user_id': user and user.id or False})
                    cashier_group = self.env.ref('odesco_access_right.group_school_cashier')
                    emp_group = self.env.ref('base.group_user')
                    user.write({'groups_id': [(6, 0, [emp_group.id, cashier_group.id])]})

        for rec in self:
            # Assign email
            if rec.is_school_cashier and rec.user_id:
                if vals.get('login'):
                    rec.user_id.write({'login': vals.get('login')})
                # Assign name
                if vals.get('name', False):
                    rec.user_id.write({'name': vals.get('name')})
                    if rec.user_id.partner_id:
                        rec.user_id.partner_id.write({'name': vals.get('name')})
                if vals.get('last_name', False):
                    if rec.user_id.partner_id:
                        rec.user_id.partner_id.write({'last_name': vals.get('last_name')})
                if vals.get('mobile_phone', False):
                    if rec.user_id.partner_id:
                        rec.user_id.partner_id.write({'phone': vals.get('mobile_phone')})
        return res

    @api.multi
    def unlink(self):
        recordset = self.env['res.users']
        ''' deleting the user related to  hr.employee which are school teachers'''
        for rec in self:
            if rec.is_school_cashier:
                user = rec.mapped('resource_id.user_id')
                recordset += user
        super(Cashier, self).unlink()
        return recordset.unlink()
