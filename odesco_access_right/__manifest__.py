{

    'name': 'Odesco Access Right',
    'summary' : "",
    'version' : "1.0",

    'author': 'Diginov Team',
    'website': 'https://diginov.tech/',
    'depends': ['base','genext_school'],

    'data': [
         'data/init_data.xml',
         'security/cashier_security.xml',
         'views/cashier.xml',
         'security/ir.model.access.csv',


    ],

    'installable': True,
    'auto_install': False,

}