{

    'name': 'Events Management',
    'summary' : "",
    'version' : "1.0",

    'author': 'Diginov Team',
    'website': 'https://diginov.tech/',
    'depends': ['school_config','registration'],

    'data': [
         'views/odesco_event_view.xml',
         'views/odesco_event_registration_view.xml',

    ],

    'installable': True,
    'auto_install': False,

}