# -*- coding: utf-8 -*-
from odoo import osv
from odoo import models, api, fields, _
import time
from odoo.exceptions import ValidationError

class oschool_event(models.Model):

    _inherit="product.product"

    academic_year = fields.Many2one('academic.year', ondelete='cascade', string="Academic year")
    event_start_date_registration = fields.Date(string='Start Registration Date')
    event_stop_date_registration = fields.Date(string='End Registration Date')
    event_date_start = fields.Date(string='Start Date')
    event_date_stop = fields.Date(string='End Date')
    event_place_number = fields.Integer(string="Number of places")


class  pos_order_line(models.Model):

    _inherit = 'pos.order.line'

    product_category_id = fields.Many2one('product.category',string='Category of Product')
    event_price = fields.Float('Prix', compute='_compute_event_price', store=True)


    @api.model
    def create(self, values):
        student_id = self.env['school.student']

        res = super(pos_order_line, self).create(values)
        period_obj = self.env['account.period']
        if 'type' in values:
            if values['type'] == 'event':
                create_date = time.strftime('%Y-%m-%d')
                period_id = period_obj.search(
                    [('date_start', '<=', create_date), ('date_stop', '>=', create_date), ('state', '=', 'draft')])
                print period_id
                if not period_id:
                    raise osv.except_osv(_('Warning!'),
                                         _('There is no period defined for this date: %s.') % create_date)
                product = self.env['product.product'].browse(values['product_id'])
                print product

                res.write({'period_id': period_id.id,
                            'parent_id': values['parent_id'],
                            'academic_year_id': product.product_tmpl_id.academic_year.id,
                            'price_unit': product.product_tmpl_id.list_price,
                            })




        return res



    def _set_remaining_place(self):

        res= super(pos_order_line, self)._onchange_product_id()



        self.academic_year_id = self.product_id.academic_year.id
        event_category_id = self.env.ref('inscription.odesco_event_product_category')
        event_registered = self.env['pos.order.line'].search([('product_category_id', '=', event_category_id.id),
                                                                  ('academic_year_id', '=', self.academic_year_id.id),
                                                                  ('qty', '!=', -1)])
            # student_registered = self.env['pos.order.line'].search([('product_id', '=', self.product_id.id), ('academic_year_id', '=', self.academic_year_id.id),('student_id', '=', self.student_id.id)])
        self.remaining_places = self.product_id.event_place_number - len(event_registered)


            # if( self.product_id.event_place_number == len(event_registered) -1):
            #     raise ValidationError("There is no more places available")
            # if(len(student_registered) - 1 > 0):
            #     raise ValidationError("Student already registered for this event")

        return res

    @api.one
    @api.depends('product_id')
    def _compute_event_price(self):

        self.event_price = self.product_id.product_tmpl_id.list_price


class oschool_event_student(models.Model):
     _inherit = 'school.student'

     event_registration_ids = fields.One2many('pos.order.line', 'student_id', string='Registration List',domain=[('type' , '=', 'event')])
