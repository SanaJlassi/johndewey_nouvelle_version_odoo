{

    'name': 'Account Period',
    'summary' : "Account Period",
    'version' : "1.0",

    'author': 'Diginov Team',
    'website': 'https://diginov.tech/',
    'depends': ['account'],
    'external_dependencies': {
        'python': [],
        'bin': [],
    },
    'data': [
         'security/account_security.xml',
         'security/ir.model.access.csv',
         'views/account_menuitem.xml',
         'views/account_views.xml'

    ],

    'installable': True,
    'auto_install': False,

}