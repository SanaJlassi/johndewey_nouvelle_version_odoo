# -*- coding: utf-8 -*-


import odoo
from odoo import api, models, _, fields, osv
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression

class account_payment(models.Model):
    _inherit = "account.payment"

    period_id = fields.Many2one('account.period', string='Period', required=False)


    @api.onchange('payment_date')
    def _onchange_paymentdate(self):
        periods = self.env['account.period'].search([])
        index=1
        for i in periods:
            if self.payment_date >= i.date_start and self.payment_date <= i.date_stop:
                self.period_id = i

    def _get_move_vals(self, journal=None):
        """ Return dict to create the payment move
        """
        journal = journal or self.journal_id
        if not journal.sequence_id:
            raise UserError(_('Configuration Error !'), _('The journal %s does not have a sequence, please specify one.') % journal.name)
        if not journal.sequence_id.active:
            raise UserError(_('Configuration Error !'), _('The sequence of journal %s is deactivated.') % journal.name)
        name = self.move_name or journal.with_context(ir_sequence_date=self.payment_date).sequence_id.next_by_id()
        return {
            'name': name,
            'date': self.payment_date,
            'ref': self.communication or '',
            'company_id': self.company_id.id,
            'journal_id': journal.id,
            'period_id': self.period_id.id,
        }


    def _get_shared_move_line_vals(self, debit, credit, amount_currency, move_id, invoice_id=False):
        """ Returns values common to both move lines (except for debit, credit and amount_currency which are reversed)
        """
        return {
            'partner_id': self.payment_type in ('inbound', 'outbound') and self.env['res.partner']._find_accounting_partner(self.partner_id).id or False,
            'invoice_id': invoice_id and invoice_id.id or False,
            'move_id': move_id,
            'debit': debit,
            'credit': credit,
            'amount_currency': amount_currency or False,
            'period_id':self.period_id.id,
        }