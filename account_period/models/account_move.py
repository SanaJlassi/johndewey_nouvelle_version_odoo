# -*- coding: utf-8 -*-


import odoo
from odoo import api, models, _, fields, osv
from odoo.osv import expression


class AccountMove(models.Model):
    _inherit = "account.move"

    period_id = fields.Many2one('account.period', 'Period', required=False, states={'posted':[('readonly',True)]})



class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    period_id = fields.Many2one('account.period', string='Period', related='move_id.period_id',required=False, index=True,store=True)



class account_invoice(models.Model):
    _inherit = "account.invoice"

    period_id = fields.Many2one('account.period','Period', required=False, states={'open':[('readonly',True)]})

    @api.onchange('date_invoice')
    def _onchange_paymentdate(self):
        periods = self.env['account.period'].search([])

        for period in periods:
            if self.date_invoice >= period.date_start and self.date_invoice <= period.date_stop:
               self.period_id = period

    @api.multi
    def invoice_validate(self):

        res = super(account_invoice, self).invoice_validate()

        if not self.period_id:
           self._onchange_paymentdate()

        acc_move = self.env['account.move'].search([('id', '=', self.move_id.id)])
        acc_move_lines = self.env['account.move.line'].search([('move_id', '=', self.move_id.id)])
        if acc_move:
            acc_move.write({'period_id':self.period_id.id})
        if acc_move_lines:
           for line in acc_move_lines:
               line.write({'period_id':self.period_id.id})
        return res