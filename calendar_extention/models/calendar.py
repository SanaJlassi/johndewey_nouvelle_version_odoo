# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
import odoo.addons.decimal_precision as dp






class MeetingType(models.Model):
    _inherit = "calendar.event.type"

    type                    = fields.Selection([('administrative','Administratif'), ('pedagogic','Pédagogique')], default='administrative', string="Type")
    state                   = fields.Selection([('waiting','En attente'),('accepted','Accepté'),('rejected','Rejeté'),('canceled','Annulé')], default='waiting', string="état")
    
    @api.multi
    def name_get(self):
        return [(record.id, record.type)for record in self]