{
    'name': 'Exams',
    'version': '1.0',
    'category': 'Scolarité',
    'author': 'Genext Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        'security/ir.model.access.csv',
        'views/report_external_layout.xml',
        'views/report_bulletin.xml',

        'views/report_studentbulletin.xml',

        'views/bulletin_view.xml',
        'views/exam_report.xml',
        'views/exam_calendar_view.xml',
        'views/module_view.xml',
        'views/exam_view.xml',
        'views/config_view.xml',
        'views/note_view.xml',
        #'views/attendance_view_inherit.xml',
        'views/comment_view.xml',
        'wizard/bulletin_report_view.xml',

        'wizard/report_bulletin.xml',

        'views/theme_bulletin.xml',
        'views/school_exams_menuitem.xml',
        'views/report_prescolaire.xml',
        
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
