# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)

class Theme(models.Model):
    
    _name="theme.bulettin"
    name     = fields.Char("Théme",required=True)
    contenu  = fields.Text("Contenu")

class key_concept(models.Model):
	_name= 'key.concept'

	name = fields.Char('Concepts clé',required=True)

class Success_indicator(models.Model):
    _name='success.indicator'
    
    c1   = fields.Selection([('little_element_observed','Peu d''éléments observé'),('some_elements_observed','Quelques éléments observés'),('meets_the_expectations','Répond aux attentes'),('exceeds_expectations','Dépasse les attentes')])
    c2   = fields.Selection([('little_element_observed','Peu d''éléments observé'),('some_elements_observed','Quelques éléments observés'),('meets_the_expectations','Répond aux attentes'),('exceeds_expectations','Dépasse les attentes')])
    c3   = fields.Selection([('little_element_observed','Peu d''éléments observé'),('some_elements_observed','Quelques éléments observés'),('meets_the_expectations','Répond aux attentes'),('exceeds_expectations','Dépasse les attentes')])
    c4   = fields.Selection([('little_element_observed','Peu d''éléments observé'),('some_elements_observed','Quelques éléments observés'),('meets_the_expectations','Répond aux attentes'),('exceeds_expectations','Dépasse les attentes')])
    c5   = fields.Selection([('little_element_observed','Peu d''éléments observé'),('some_elements_observed','Quelques éléments observés'),('meets_the_expectations','Répond aux attentes'),('exceeds_expectations','Dépasse les attentes')])
    theme_theme_id = fields.Many2one('theme.theme')
    student_id     = fields.Many2one('school.student', string="élève", required=True, readonly=True)


class Theme_theme(models.Model):
    _name="theme.theme"
     
    educational_stage_id                = fields.Many2one('educational.stage', string="Cycles", required=True)
    grade_id                            = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]", required=True)
    class_id                            = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)

    
    theme = fields.Many2one('theme.bulettin', required=True)
    teacher_idea = fields.Text('Idée maitresse')    
    research_track = fields.Text('Pistes de recherche')
    key_concept   = fields.Many2many('key.concept')
   
    success_indicator_ids = fields.One2many('success.indicator','theme_theme_id')

    @api.onchange('class_id')
    def _onchange_class_id(self):
        comment_list = []
        if len(self.class_id):
            student_id = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in student_id:
                comment_list.append(({'student_id':student.id,'c1':None,'c2':None,'c3':None,'c4':None,'c5':None}))
               
        return {'value':{'success_indicator_ids':comment_list}}

class TeacherCommenttheme(models.Model):
    _name = 'teacher.comment.theme'
    _description = 'Commentaire enseignant par theme'

    name                                = fields.Char('Commentaire')
    educational_stage_id                = fields.Many2one('educational.stage', string="Cycles", required=True)
    grade_id                            = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]", required=True)
    class_id                            = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)


    annual_calendar_id                  = fields.Many2one('annual.calendar', string="Année scolaire", required=True, default=lambda self: self.env['annual.calendar'].get_current_year())
    semestre_id                         = fields.Many2one('annual.periode', domain="[('educational_stage_id','=',educational_stage_id)]", string="Semestre", required=True)
    #periode_id                          = fields.Many2one('annual.periode', domain="['&',('last_level','=',True),('parent_id','=',semestre_id)]", string="Evaluation")

    theme                              = fields.Many2one('theme.bulettin', string="Théme")
    teacher_id                          = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", string="Enseignant", 
                                    default=lambda self: self.env['hr.employee'].search([('partner_id','=',self.env.user.partner_id.id)]))
    teacher_comment_line_idss            = fields.One2many('admin.comment.line','teacher_comment_theme_id', string="Commentaires")


    @api.onchange('class_id')
    def _onchange_class_id(self):
        comment_list = []
        if len(self.class_id):
            student_id = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in student_id:
                comment_list.append((0,0,{'student_id':student.id, 'comment':None}))
        return {'value':{'teacher_comment_line_idss':comment_list}}

    #@api.onchange('theme_id')
    #def _onchange_theme(self):
        #comment_list = []
       # if len(self.theme_id):
          #prev_comments = self.env['class.note'].search([('theme_id','=',self.theme_id.id),('class_id','=',self.class_id.id),('grade_id','=',self.grade_id.id),('semestre_id','=',self.semestre_id.id),('annual_calendar_id','=',self.annual_calendar_id.id),('educational_stage_id','=',self.educational_stage_id.id)])
          #comments = prev_comments.mapped('comment_line_ids').filtered(lambda r: r.comment != False)

          #print "######### NEW COMMENTS",comments
          #print "######### NEW COMMENTS",prev_comments
          #for line in comments:
            #print "##### comment ",line.comment
            #print "### student ", line.student_id.name
            #comment_list.append((0,0,{'student_id':student_id.id, 'comment':None}))
        #return {'value':{'teacher_comment_line_idss':comment_list}}