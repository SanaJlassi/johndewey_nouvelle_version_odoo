# -*- coding: utf-8 -*-

from odoo import fields,api, models, _
from odoo.exceptions import UserError, ValidationError

class StudentBulletinObject(models.Model):
    _name = "student.bulletin.object"
    _description = "Bulletin"



    grade_id            = fields.Many2one('school.grade', 'Niveau scolaire', required=True)
    class_id            = fields.Many2one('school.class', 'Classe', domain="[('grade_id','=',grade_id)]", required=True)
    annual_calendar_id  = fields.Many2one('annual.calendar', 'Années scolaire',  required=True)
    periode_id          = fields.Many2one('annual.periode', 'Semestre', required=True, domain="['&',('annual_calendar_id','=',annual_calendar_id),('last_level','=',False)]")
    step_id             = fields.Many2one('annual.periode', 'étape', domain="['&',('parent_id','=',periode_id),('last_level','=',True)]")
    student_id          = fields.Many2one('school.student')






class ReportBulletin(models.TransientModel):
    _name = "student.bulletin.wizard"
    _description = "Bulletin"

    educational_stage_id =fields.Many2one('educational.stage','Niveau educative',required=True)
    
    grade_id            = fields.Many2one('school.grade', 'Niveau scolaire', required=True,domain="[('educational_stage_id','=',educational_stage_id)]")
    class_id            = fields.Many2one('school.class', 'Classe', domain="[('grade_id','=',grade_id)]", required=True)
    
    annual_calendar_id  = fields.Many2one('annual.calendar', 'Années scolaire',  required=True)
    periode_id          = fields.Many2one('annual.periode', 'Semestre', required=True, domain="['&',('annual_calendar_id','=',annual_calendar_id),('educational_stage_id','=',educational_stage_id),('last_level','=',False)]")
    step_id             = fields.Many2one('annual.periode', 'étape', domain="['&',('parent_id','=',periode_id),('last_level','=',True)]")

    # student_id          = fields.Many2many('school.student', domain="[('class_id','=',class_id)]",  string="Elèves" )
    student_id         = fields.Many2many('school.student', domain="[('class_id','=',class_id)]", string="Elèves") 
    # student_id          = fields.Many2many('school.student', compute="_compute_student",  string="Elèves" )

    # @api.depends('class_id')
    # def _compute_student(self):
    #     for rec in self:
    #         rec.student_id = rec.class_id.student_ids

    

    @api.multi
    def _compute_periode(self):
        if self.grade_id:
            educational_stage_id = self.grade_id.educational_stage_id
            annual_calendar_id=self.annual_calendar_id
            period_config_ids=self.env['periode.config'].search(['&',('educational_stage_id','=',educational_stage_id.id),('annual_calendar_id','=',annual_calendar_id.id)])
            periode_id = period_config_ids.mapped('periode_ids')
            print"#########",periode_id

    @api.multi
    def create_report_object(self):
        for rec in self:
            for student in rec.class_id.student_ids:
                recorde = self.env['student.bulletin.object'].create(self.prepare_dict(rec,student))
                print "####### CREATED RECORD ",recorde.grade_id.name




    def prepare_dict(self, record, student):
        return {
            'grade_id':record.grade_id.id,
            'class_id':record.class_id.id,
            'annual_calendar_id':record.annual_calendar_id.id,
            'periode_id':record.periode_id.id,
            'step_id':record.step_id.id,
            'student_id':student.id,
        }


    
    @api.onchange('grade_id','annual_calendar_id')
    def _onchange_grade_id(self):
        if self.grade_id:
            educational_stage_id = self.grade_id.educational_stage_id
            annual_calendar_id=self.annual_calendar_id
            periode_id = self.env['annual.periode'].search(['&',('educational_stage_id','=',educational_stage_id.id),('annual_calendar_id','=',annual_calendar_id.id)])
            print "#### ",periode_id 
            return {'domain': {'periode_id': [('id', 'in', [r.id for r in periode_id])]}}
            


    def _build_contexts(self, data):
        result = {}
        result['grade_id'] = data['form']['grade_id'] or False
        result['class_id'] = data['form']['class_id'] or False
        result['annual_calendar_id'] = data['form']['annual_calendar_id'] or False
        result['periode_id'] = data['form']['periode_id'] or False
        result['step_id'] = data['form']['step_id'] or False
        result['student_id'] = data['form']['student_id'] or False
        print "###### _build_contexts"
        return result

    def _print_report(self, data):
        print "######## PRINT REPORT ",data
        return self.env['report'].get_action(self, 'school_exams.report_studentbulletin', data=data)

    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['grade_id', 'class_id','annual_calendar_id', 'periode_id', 'step_id','student_id'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        print "###### check_report"

        # self.create_report_object()
        return self._print_report(data)
        # print "wizard report", len(report)
        # print "wizard report", type(report)
        # print "wizard report", report
        # return report
