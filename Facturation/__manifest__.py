{
    'name': 'Facturation',
    'version': '1.0',
    'category': 'School Management',
    'author': 'Genext Team',
    'website': 'https://www.genext-it.com',
    'depends': ['base','hr','account'],
    'data': [
        'views/menuitem.xml',
      
        ],

    'installable': True,
    'auto_install': False,
    'application': True,
  }