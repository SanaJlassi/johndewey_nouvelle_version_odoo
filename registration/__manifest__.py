{

    'name': 'Registration',
    'summary' : "",
    'version' : "1.0",

    'author': 'Diginov Team',
    'website': 'https://diginov.tech/',
    'depends': ['genext_school','point_of_sale', 'account'],

    'data': [

         'views/registration_view.xml',
         'views/payement_view.xml',
         'views/odesco_all_sessions_view.xml',
         'views/odesco_session_view.xml',
         'views/pos_order_view.xml',
         'data/point_of_sale_data.xml'


    ],

    'installable': True,
    'auto_install': False,

}