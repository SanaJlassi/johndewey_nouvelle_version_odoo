# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import uuid

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class PosConfig(models.Model):
    _inherit = 'pos.config'

    @api.multi
    def open_ui(self):


        obj = self.env.ref('genext_school.root_menu')
        return {
            'type': 'ir.actions.client',
            'name': 'Point of Sale Menu',
            'tag': 'reload',
            'params': {'menu_id': obj.id},
        }
        return  super(PosConfig, self).open_ui()

    @api.multi
    def open_existing_session_cb_close(self):

        assert len(self.ids) == 1, "you can open only one session at a time"
        if self.current_session_id.cash_control:
            self.current_session_id.action_pos_session_closing_control()
        return self.open_session_cb()
        return super(PosConfig, self).open_existing_session_cb_close()

    @api.multi
    def open_session_cb(self):

        assert len(self.ids) == 1, "you can open only one session at a time"
        if not self.current_session_id:
            self.current_session_id = self.env['pos.session'].create({
                'user_id': self.env.uid,
                'config_id': self.id
            })
            if self.current_session_id.state == 'opened':
                return self.open_ui()
            return self._open_session(self.current_session_id.id)
        return self._open_session(self.current_session_id.id)
        return super(PosConfig, self).open_session_cb()

    @api.multi
    def open_existing_session_cb(self):

        assert len(self.ids) == 1, "you can open only one session at a time"
        return self.odesco_open_session(self.current_session_id.id)
        return super(PosConfig, self).open_existing_session_cb()


    def _open_session(self, session_id):

        return {
            'name': _('Session'),
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'pos.session',
            'res_id': session_id,
            'view_id': False,
            'type': 'ir.actions.act_window',
        }

        return  super(PosConfig, self)._open_session()