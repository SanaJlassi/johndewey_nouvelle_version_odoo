# -*- coding: utf-8 -*-
from datetime import timedelta, datetime
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError, UserError
from dateutil.relativedelta import relativedelta
from odoo.osv import osv

from odoo.exceptions import ValidationError
from lxml import etree
import time
import odoo.addons.decimal_precision as dp


class Student(models.Model):
    _inherit='school.student'

    academic_year = fields.Many2one('academic.year', string="Année scolaire")
    group_id = fields.Many2one('school.grade', string='Niveau scolaire')
    class_id = fields.Many2one('school.class', string='Classe')
    @api.multi
    def registration_student(self):

        view_id = self.env.ref('registration.view_student_registration_dialog_form')

        return {
            'name': _("Registration Student"),
            'view_mode': 'form',
            'view_id': view_id.id,
            'view_type': 'form',
            'res_model': 'pos.order.line',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'default_student_id': self.id,
                'default_type': 'registration',
                'type': 'registration'
            }
        }

    @api.multi
    def unlink(self):
        raise UserError(_('Impossible de supprimer Élève'))

        return super(Student, self).unlink()




    registration_ids = fields.One2many('pos.order.line', 'student_id', 'Registration List',
                                        domain=[('type', '=', 'registration')])

    #academic_year = fields.Many2one('academic.year', string="Année scolaire")
    


class res_partner(models.Model):
    _inherit = 'res.partner'


    @api.multi
    def payment_responsible(self):


        view_id = self.env.ref('odesco_study.view_payment_dialog_form')

        return {
            'name': _("Payement"),
            'view_mode': 'form',
            'view_id': view_id.id,
            'view_type': 'form',
            'res_model': 'odesco.payment',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'default_responsible_id': self.id,
            }
        }


    registration_ids = fields.One2many('pos.order.line', 'parent_id', 'Registration List',
                                       domain=[('type', '=', 'registration')])
    
    @api.model
    def create(self, vals):
        property_account_receivable = self.env['account.account'].search([('code', '=', '411100')])
        property_account_payable = self.env['account.account'].search([('code', '=', '401100')])

        res = super(res_partner, self).create(vals)
        res.write({'property_account_receivable_id': property_account_receivable.id,
                   'property_account_payable_id': property_account_payable.id, })
        return res
    


class pos_order_line(models.Model):
    _inherit = 'pos.order.line'
    _order = 'product_id'




    order_id = fields.Many2one('pos.order', 'Order Ref', ondelete='set null')
    state_order = fields.Selection(related='order_id.state', string='Order State', store=True)
    student_id = fields.Many2one('school.student', 'Élève', ondelete='restrict')
    parent_id = fields.Many2one('res.partner','Responsable', store=True)
    period_id =  fields.Many2one('account.period', 'Period')
    date_start =  fields.Date(related='period_id.date_start', string='Start of Period')
    class_id = fields.Many2one('school.class', 'Class')
    group_id = fields.Many2one('school.grade', 'Niveaux scolaire')
    academic_year_id = fields.Many2one('academic.year', 'Année scolaire', store=True)
    state_academic_year = fields.Selection(related='academic_year_id.state', string='State Academic year', store=True)
    type = fields.Char('Type', size=64)
    refunded = fields.Boolean('Refunded')
    registration_price = fields.Float('Prix', store=True)
    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', states={'draft': [('readonly', False)]})





    @api.model
    def create(self, values):

        student_id = self.env['school.student']
        res = super(pos_order_line, self).create(values)
        period_obj = self.env['account.period']
        if 'type' in values:
            if values['type'] == 'registration':
                create_date = time.strftime('%Y-%m-%d')
                period_id = period_obj.search([('date_start', '<=', create_date), ('date_stop', '>=', create_date), ('state', '=', 'draft')])

                if not period_id:
                    raise osv.except_osv(_('Warning!'), _('There is no period defined for this date: %s.') % create_date)
                product = self.env['product.product'].browse(values['product_id'])
                student = student_id.browse(values['student_id'])


                academic_year = self.env['academic.year'].browse(values['academic_year_id'])

                values['price_unit'] = product.product_tmpl_id.list_price

                res.write({'period_id': period_id.id,
                    'parent_id': values['parent_id'],
                    'academic_year_id': product.product_tmpl_id.academic_year.id,
                    'registration_price': product.product_tmpl_id.list_price,
                    'price_unit': values['registration_price'],

                             })

                state_academic_year = product.pos_categ_id.academic_year.state
                if not student.academic_year_id or state_academic_year == 'current':

                     student.write({'academic_year': product.pos_categ_id.academic_year.id})

                if not student.grade_id or state_academic_year == 'current':

                     student.write({'group_id': values['group_id']})
                if not student.class_id and values['class_id'] or state_academic_year == 'current':

                     student.write({'class_id': values['class_id']})


        return res

    @api.multi
    def unlink(self):
        for rec in self.browse(self.ids):
            if rec.type == 'registration':
                if rec.order_id and rec.order_id.state != 'draft':
                    raise osv.except_osv(_('Unable to Delete!'), _('Can not delete the order line %s' % (rec.product_id.name,)))
                else:
                    rec.student_id.write({'group_id': False, 'class_id': False, 'academic_year':False})
            elif (rec.order_id and rec.qty > 0) or (rec.order_id and rec.order_id.state != 'draft'):
                raise osv.except_osv(_('Unable to Delete!'), _('Can not delete the order line %s' % (rec.product_id.name,)))
        return super(pos_order_line, self).unlink()

    @api.multi
    def action_payment(self):

        pos_ref = self.env['pos.order']
        pos_line_ref = self.env['pos.order.line']
        product_o = self.env['product.product']
        journal_obj = self.env['account.journal']
        pos_ids = []

        for line in self.env['pos.order.line'].browse(self.ids):
            if line.order_id:
                inv_id = line.order_id
            else:

                inv = {'partner_id': line.parent_id.id,
                       'student_id': line.student_id.id,


                       }
                inv_id = pos_ref.create(inv)
                journal_registration = journal_obj.search([('registration', '=', True)])
                if journal_registration:
                    self._cr.execute('update pos_order set sale_journal = %s where id = %s',
                               (journal_registration.id, inv_id.id))

                self.write({'order_id': inv_id.id})

        #mod_obj = self.env['ir.model.data']
        res = self.env.ref('registration.view_odesco_registration_pos_form')

        return {
            'name': _('Payment Registration'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'type': 'ir.actions.act_window',
            'context': [('id', '=', inv_id.id)],
            'view_id': res.id,
            'res_id': inv_id.id,


        }

    def button_registration(self, cr, uid, ids, context=None):
        return True

    @api.multi
    @api.onchange('product_id','student_id')
    def _onchange_product_id(self):



        reg = self.env['pos.order.line'].search(['&', ('type', '=', 'registration'),
                                                 ('academic_year_id', '=', self.product_id.product_tmpl_id.academic_year.id),
                                                 ('student_id', '=', self.student_id.id), ('qty', '!=', -1)])

        if len(reg) > 0:
            return {'value': {'product_id': False},
                    'Warning': {'title': _('Warning!'), 'message': _('Student already registered.')}}

        self.academic_year_id= self.product_id.product_tmpl_id.academic_year.id
        self.registration_price =self.product_id.product_tmpl_id.list_price






    @api.multi
    @api.onchange('student_id')
    def _onchange_student(self):

        self.parent_id = self.student_id.legal_responsable

   



class pos_order(models.Model):
    _inherit = 'pos.order'

    def _get_pos_from_lines(self):
        line_obj = self.env['pos.order.line']
        return [line.order_id.id for line in line_obj.browse()]

    def _get_journal(self):
        result = dict.fromkeys(self.ids, False)
        journal_obj = self.env['account.journal']
        for pos_order in self.browse():
            journal_id = pos_order.session_id.config_id.journal_id.id
            for line in pos_order.lines:
                if line.type == 'registration':
                    if not journal_obj.search([('registration', '=', True)], limit=1):
                        raise osv.except_osv(_('Error!'), _('No registration journal type'))
                    journal_id = journal_obj.search([('registration', '=', True)], limit=1)[0]
                elif line.product_id.cash and pos_order.partner_id.cash:
                    journal_id = journal_obj.search([('cash', '=', True)], limit=1)[0]
            result[pos_order.id] = journal_id
        return result

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):

        if self._context is None: context = {}
        if view_type == 'form':
            if self._context.get('pos_id') == 'pos':
                result = self.env.ref('registration.view_odesco_registration_pos_form')

                view_id = result.id

        res = super(pos_order, self).fields_view_get(view_id=view_id, view_type=view_type,
                                                     toolbar=toolbar, submenu=submenu)
        return res

    @api.multi
    def unlink(self):
        for rec in self.browse():
            if rec.lines[0].type == 'registration' and rec.state == 'draft':
                self.env['pos.order.line'].unlink(rec.lines[0].id)
        return super(pos_order, self).unlink()

    # @api.multi
    # def _create_account_move(self, dt, ref, journal_id, company_id):
    #
    #     res = super(pos_order, self)._create_account_move(dt, ref, journal_id, company_id)
    #     date_tz_user1 = fields.Datetime.context_timestamp(self, fields.Datetime.from_string(dt))
    #     date_tz_user = fields.Date.to_string(date_tz_user1)
    #     period_id = self.env['account.period'].search([('date_start', '<=', dt), ('date_stop', '>=', dt)])
    #     print 'oussama'
    #     print period_id
    #     return self.env['account.move'].sudo().create({'ref': ref, 'journal_id': journal_id, 'date': date_tz_user,
    #                                                    'period_id': period_id.id})
    #     return  res

    



    def _default_sesssion(self):
        session = self.env['pos.session'].search([('state', '=', 'opened'), ('user_id', '=', self.env.uid)], limit=1)
        if not session:
           raise UserError(
                _("il faut ouvrir une caisse"))
        return session

    sale_journal = fields.Many2one('account.journal',compute='_get_journal',  string="Sale Journal", store=True)
    student_id = fields.Many2one('school.student', string="Student")
    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist',  required=False)
    session_id = fields.Many2one(
        'pos.session', string='Session', required=True, index=True,
        domain="[('state', '=', 'opened')]", states={'draft': [('readonly', False)]},
        readonly=True, default=_default_sesssion)
    



    @api.model
    def _prepare_bank_statement_line_payment_values(self, data):
        """Create a new payment for the order"""
        args = {
            'amount': data['amount'],
            'date': data.get('payment_date', fields.Date.today()),
            'name': self.name + ': ' + (data.get('payment_name', '') or ''),
            'partner_id': self.env["res.partner"]._find_accounting_partner(self.partner_id).id or False,
            'num_check':data['num_check'],
            'check_holder':data['check_holder'],
            'bank_name':data['bank_name'],
            'rib':data['rib'],
            'due_date':data['due_date'],
            'company_name':data['company_name'],


        }
        print"############num check",data['num_check']

        journal_id = data.get('journal', False)
        statement_id = data.get('statement_id', False)
        assert journal_id or statement_id, "No statement_id or journal_id passed to the method!"

        journal = self.env['account.journal'].browse(journal_id)
        # use the company of the journal and not of the current user
        company_cxt = dict(self.env.context, force_company=journal.company_id.id)
        account_def = self.env['ir.property'].with_context(company_cxt).get('property_account_receivable_id', 'res.partner')
        args['account_id'] = (self.partner_id.property_account_receivable_id.id) or (account_def and account_def.id) or False

        if not args['account_id']:
            if not args['partner_id']:
                msg = _('There is no receivable account defined to make payment.')
            else:
                msg = _('There is no receivable account defined to make payment for the partner: "%s" (id:%d).') % (
                    self.partner_id.name, self.partner_id.id,)
            raise UserError(msg)

        context = dict(self.env.context)
        context.pop('pos_session_id', False)
        for statement in self.session_id.statement_ids:
            if statement.id == statement_id:
                journal_id = statement.journal_id.id
                break
            elif statement.journal_id.id == journal_id:
                statement_id = statement.id
                break
        if not statement_id:
            raise UserError(_('You have to open at least one cashbox.'))

        args.update({
            'statement_id': statement_id,
            'pos_statement_id': self.id,
            'journal_id': journal_id,
            'ref': self.session_id.name,
        })

        return args

class odesco_account_journal(models.Model):
    _inherit = 'account.journal'

    cash = fields.Boolean(default=False, string="Liquidité")
    registration = fields.Boolean(default=False, string="Inscription")
    #is_check = fields.Boolean(default=False, string="Is check")

class odesco_account_bank_statement_line(models.Model):
    _inherit= 'account.bank.statement.line'
    num_check   = fields.Char(string="Numéro du chéque")
    check_holder                  = fields.Char(string="Titulaire du chéque")
    bank_name                     = fields.Char(string="Banque")
    rib                           = fields.Char(string="RIB")
    due_date                      = fields.Date(string="Date d'échéance")
    company_name                  = fields.Char(string="Société")
    type                          = fields.Selection(related="journal_id.type")
    
class odesco_make_payement(models.TransientModel):
    _inherit = 'pos.make.payment'
    
    journal_id = fields.Many2one('account.journal', string='Payment Mode', required=True)

                                #,domain="[('type', 'in', ['bank', 'cash'])]")

    num_check                     = fields.Char(string="Numéro du chéque")
    check_holder                  = fields.Char(string="Titulaire du chéque")
    bank_name                     = fields.Char(string="Banque")
    rib                           = fields.Char(string="RIB")
    due_date                      = fields.Date(string="Date d'échéance")
    company_name                  = fields.Char(string="Société")
    type                          = fields.Selection(related="journal_id.type")
    
    @api.model
    def create(self,vals):
        payment= super(odesco_make_payement,self).create(vals)
       
        return payment


