# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)


class Exam(models.Model):
    _name = 'school.exam'
    _description = 'Exam'

    name                        = fields.Char(string="Nom de l'examen", required=True)

    from_date                   = fields.Datetime(string="Date de début", default=fields.Datetime.now(), required=True)
    to_date                     = fields.Datetime(string="Date de fin", default=fields.Datetime.now(), required=True)
    exam_type                   = fields.Many2one('school.exam.type',  string="Type de l'examen", required=True)
    note_max                    = fields.Integer(string="Note maximale", required=True)

    grade_id                    = fields.Many2one('school.grade',  string='Niveau scolaire', required=True)
    class_id                    = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", string="Classe", required=True)
    subject_id                  = fields.Many2one('school.subject',  string='Matière', required=True)
    teacher_id                  = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", ondelete='set null', string="Enseignant", required=True)

    educational_stage_id        = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
    timing_system_id            = fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
    timing_system_periode_id    = fields.Many2one('school.timing.system.period', domain="[('timing_system_id','=',timing_system_id)]", ondelete='set null', string="Période")
    
    exam_notes_id               = fields.One2many('school.exam.note', 'exam_id', ondelete='cascade')

    description                 = fields.Char(string="Description de l'examen", default="Description")
    # exam_note_line_ids          = fields.One2many('school.exam.note.line', compute='_compute_note')
    state                       = fields.Selection([('unapproved','Non approuvé'),('approved','Approuvé'),], default='unapproved', string="état")

    @api.one
    def set_unapproved(self):
        self.write({'state':'unapproved'})        

    @api.one
    def set_approved(self):  
        self.write({'state':'approved'})


    @api.onchange('class_id')
    def _onchange_class_id(self):
        return {'domain':{'subject_id':[('id','in',self.class_id.grade_id.subject_ids.ids)]}}




class ExamType(models.Model):
    _name = 'school.exam.type'
    _description = 'Exam Type'

    name                        = fields.Char(string="Type de l'examen", required=True)
    coefficient                 = fields.Float(string='Coefficient', default=0.0, required=True)
    



class ExamNote(models.Model):
    _name = 'school.exam.note'
    _description = 'Exam Note'


    exam_id                     = fields.Many2one('school.exam', ondelete='cascade', string="Nom de l'examen", required=True )
    subject_id                  = fields.Many2one(related='exam_id.subject_id', string='Matière', required=True)

    academic_year               = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) , required=True)
    grade_id                    = fields.Many2one(related='exam_id.grade_id', string='Niveau scolaire', required=True)    
    class_id                    = fields.Many2one(related='exam_id.class_id', string='Classe', required=True)
    educational_stage_id        = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
    timing_system_id            = fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
    period                      = fields.Many2one(related='exam_id.timing_system_periode_id', string='Période', required=True)
    session                     = fields.Selection([('rattrapage','Rattrapage'),('principale','Principale')], default='principale', string="Session")
    
    teacher_id                  = fields.Many2one(related='exam_id.teacher_id', string='Enseignant', required=True)
    student_ids                 = fields.Many2many('school.student', string="Élève", domain="[('class_id','=',class_id)]", required=True)
    datetime                    = fields.Datetime(default=fields.Datetime.now(), string="Date")

    state                       = fields.Selection([('unapproved','Non approuvé'),
                                                                 ('approved','Approuvé'),
                                                                 ('ignored','Ignoré'),
                                                                 ], default='unapproved', string="état")
    moyenne                     = fields.Float(string="La moyenne de la classe", default=0.0)

    exam_notes_line_ids         = fields.One2many('school.exam.note.line', 'exam_notes_id')


    # @api.multi
    # def name_get(self):
    #     return [(record.id, record.subject_id.name +'/'+ record.name)for record in self]



    @api.one
    def set_unapproved(self):
        self.write({'state':'unapproved'})        

    @api.one
    def set_approved(self):  
        self.write({'state':'approved'})


    @api.one
    def set_ignore(self):
        self.write({'state':'ignored'})

    @api.onchange('class_id')
    def _onchange_class_id(self):
        student_line= []
        if len(self.class_id):
            students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in students:
                student_line.append((0,0,{'name':'name','student_id':student.id, 'note':None, 'comment':None}))
        else:
            print "NO CLASS ID"    
        return {'value':{'exam_notes_line_ids':student_line}}




class ExamNoteLine(models.Model):
    _name = 'school.exam.note.line'
    _description = 'Exam Notes Line'

    name                        = fields.Char(string="Note",default="exam note line",readonly=True)

    exam_notes_id               = fields.Many2one('school.exam.note')
    student_id                  = fields.Many2one('school.student', string="Élève")
    note                        = fields.Float(string='Notes', default=0.0)
    comment                     = fields.Char(string="Commentaire")


class ExamReport(models.Model):
    _name = 'school.exam.report'

    name = fields.Char('Nom')

    academic_year               = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) , required=True)
    grade_id                    = fields.Many2one('school.grade',  string='Niveau scolaire', required=True)
    class_id                    = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", string="Classe", required=True)

    educational_stage_id        = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
    timing_system_id            = fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
    timing_system_periode_id    = fields.Many2one('school.timing.system.period', domain="[('timing_system_id','=',timing_system_id)]", ondelete='set null', string="Période")

    student_ids                 = fields.Many2many('school.student', string="Élève", domain="[('class_id','=',class_id)]", required=True)

